## 💬 UX/Open Source Events 2021(All Remote)

| Event | Date | Cost | Interest | CFP closing |
|-------|------|------|----------|---------|
| [techknowday](https://t.co/0BmzjCFCD2?amp=1) | March 8, 2021 | - | CFP - [recording of the talk on youtube.com](https://www.youtube.com/watch?v=XjwLFy4iJQ4&list=PL6TuxoG36L8mrnF_MqXC7OZIrYDOI_FrD&index=42&ab_channel=TECHKNOW) | Closed |
| [Open Source Summit](https://events.linuxfoundation.org/open-source-summit-europe/) | Sept 28- Oct 1, 2021 | - | CFP(in collaboration with PM) | June 13 |
| [User Research London](https://www.userresearchlondon.com/) | TBD | - | Want to attend | TBD |
| [Push UX](https://push-conference.com/) | Oct 21-22, 2021 | - | Want to attend + CFP(Misses) | April 9, 2021 |
| [Figma CONFIG](https://config.figma.com/) | Apr 21, 2021 | - | 🤔 | April 21, 2021 |
| [The Bad Conference](https://www.thebadconference.com/) | April 14-16th | 119 GBP | Want to attend |  April 14-16, 2021 |
| [Gitlab Commit 2021](https://twitter.com/gitlab/status/1367218763096084480?s=20) | April 14-16th, 2021 | - | [CFP](https://docs.google.com/forms/d/e/1FAIpQLSdYAnbBeMezbC_ZeqcTo31UHEKRhhem0wxEalM7Oy9KS3-j0w/viewform) | April 30, 2021  |
| [Re:publica](https://re-publica.com/en/) | May 20-22, 2021 | - | CFP(Missed) |  March 21, 2021 |
| [RightsCon](https://www.rightscon.org/program/) | Monday, June 7 to Friday, June 11, 2021 | - | CFP(Done) |  March 21, 2021 |
| [All things Open 2021](https://2021.allthingsopen.org/) | Sun, 17 Oct, 2021 – Tue, 19 Oct, 2021 | - | CFP(Done) |  April 30, 2021 |


## 📚 UX, Design and Product Management related book to read in 2021
| Book | Summary | Read? |
|-------|--------|-------|
| Hacking Growth |    |  ✅ |
| Super Thinking |    |  Reading |
| Superhuman by Design: Keys to Unlocking Your Creativity for Life-Changing Results |  |  |
| Play like a Feminist. (Playful Thinking) |  | |
| Thinking, Fast and Slow |  | Reading |
| The Build Trap |  | ✅ |
| Unlocking Customer Value Chain |  | ✅ |
| Inspired: How to Create Tech Products Customers Love |  | ✅ |
| EMPOWERED: Ordinary People, Extraordinary Products |  |  |
| Invisible Solutions: 25 Lenses That Reframe and Help Solve Difficult Business Problems |  | ✅ |
| Design Unbound: Designing for Emergence in a White Water World, Volume 1: Designing for Emergence (Infrastructures) |  | ✅ |

