# 👋 Hi there, I'm Veethika.

😄 Pronouns: she/her

🐦 Twitter: @veethikaa

🌏 Website: http://veethika.com/

---------------

## Who am I?
A 🐶 person who equally enjoys watching 🐈 videos. A Product Designer based out of 🇮🇳. A 70% Yoga and 30% gym believer.

## My role at GitLab
I'm a Senior Product Designer with the Verify::Continuous Integration team at Gitlab.
Read more about my [role](https://about.gitlab.com/job-families/engineering/product-designer/#senior-product-designer) and the [stage-group(Continuous Integration)](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/ci-cd/continuous-integration/).

## How I work?
Here's a [living documentation](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/ci-cd/continuous-integration/) outlining my process that I try to keep up to date all the time.

## My journey as a designer
My design journey has been anything but straightforward. I pursued my graduation in Advertising + Graphic design and started to work for advertising agencies while in the third semester itself and mostly learnt on the job. After a considerable period of working as a visual designer for advertising agencies and graphic design studios, besides doing a second graduation on the side in psychology, I decided to level-up my relationship with design and enrolled for Masters in Toy and Game Design at the National Institute of design, India. 
The prospects were not definitive but I thoroughly enjoyed my studies and started to appreciate the value of Play in everyday life. In the same phase I was also introduced to the world of open source the principles that Open Source movement was based on intrigued me. I slowly began to incorporate the principles of openness in my design processes.
My next goal was to find a job where I could do what I love the most--Design, and do it the open source way. The pursuit wasn't easy but it eventually happened. I after a couple of jobs as a UX designer, I joined an open organisation as a designer and the chain of events that followed from there finally led me to GitLab.


## My Interests
When I'm not working and not thinking about work, I love to illustrate on my iPad, watch socio-political documentaries, cook, read, play board games and keep my house-plants alive🌵. COVID made me quit my Hindustani classical training (vocals) and I plan to get back to it in soon in some capacity 🙂


## My parallel life?
There are times when I love to engage in activities which are more hands-on and demand me to pick up a tool or two from the workshop and put my wood and metal skills to test. 
