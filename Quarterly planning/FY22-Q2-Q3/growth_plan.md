# Veethika - FY22-Q2 + FY22-Q3 Growth Plan

Milestones - [14.0](https://gitlab.com/gitlab-org/gitlab/-/issues/331681)  [14.1](https://gitlab.com/gitlab-org/gitlab/-/issues/332435)  [14.2](https://gitlab.com/gitlab-org/gitlab/-/issues/332437)  [14.3](https://gitlab.com/gitlab-org/gitlab/-/issues/335073)  [14.4](https://gitlab.com/gitlab-org/gitlab/-/issues/336563)  [14.5](https://gitlab.com/gitlab-org/gitlab/-/issues/336562)

### Division of scope for Q3
| Area | Percentage | Comments |
|------|------------|----------|
| **PE Milestone Priorities** | **40%** |  |
|   - Design for scheduled issues | 15% |  |
|   - Stage Group Research | 15% |  |
|   - Strategic Planning | 10% |  |
| **UX KRs** | **15%** |  |
| **Cross-stage collaboration** | **10%** |  |
| **Self Growth & Learning** | **10%** |  |
| **Admin Work** | **10%** |  |
| Onboarding buddy responsibilities | 10% |  |
| **Buffer** | **5%** |  |

A part of the onboarding-buddy responsibilities are represented under UX KRs as well. 


---------
``` 
Overarching objective: Improve subject matter expertise to facilitate cross stage collaboration 

Q2 focus: Exercise scope expertise to identify logical gaps in experience in CI
Q3 focus: Enhance business knowledge around Verify to prioritize strategic innovation

```

<details><summary>FY 22 Q2</summary>

[CI FY22-Q2 Priorities](https://gitlab.com/gitlab-org/ci-cd/continuous-integration/-/issues/74)

### Priority areas

- **Vision:** Exert significant influence on the overall objectives and long-range goals of your section by actively contributing to the Vision pages with Product and Engineering
  - With change in leadership, re-align the product vision in collaboration with product
- **Efficiency:** 
   * Models a culture of efficiency within the team where people make good, timely decisions using available data and assessing multiple alternatives
   * Models using boring solutions for increasing the speed of innovation for our organization and product

-------

## 🚀 Action Plan
-----

* [ ] [KR: Focus on negative SUS theme related to merge requests => 0/?, 0%](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/11204)
    - [ ] https://gitlab.com/gitlab-org/gitlab/-/issues/229738
    - [ ] https://gitlab.com/gitlab-org/gitlab/-/issues/283895
    - [ ] https://gitlab.com/gitlab-org/gitlab/-/issues/283899
    - [ ] https://gitlab.com/gitlab-org/gitlab/-/issues/283903
    - [ ] https://gitlab.com/gitlab-org/gitlab/-/issues/330851
    - [ ] https://gitlab.com/gitlab-org/gitlab/-/issues/330841
    - [ ] https://gitlab.com/gitlab-org/gitlab/-/issues/330850
    - [ ] Solution Validation for [auto-merge proposal](https://gitlab.com/gitlab-org/gitlab/-/issues/325529)

-------      

* [ ]  Personal KR: Draw up a plan for time investment in Q2 to exemplify effective prioritization (early)
    - [ ] Provide a retrospection around the same towards the end of the quarter

* [ ]  Learn more about the impact of Behavioural science on design
    - [ ] Take course/watch videos
    - [ ] explore frameworks and propose their use for defining teh baseline experience for CI

--------      

* [ ] [KR: Improve product usability by understanding how to create clear, concise UI text => 1/53, 2%](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/11360)
    - [ ] Understand and learn how to write effective UI copy: Read the book [Microcopy](https://smile.amazon.com/gp/product/B071S54VLL/ref=ppx_yo_dt_b_d_asin_title_o02?ie=UTF8&psc=1) 

---------      

* [ ] [KR: Perform competitor evaluation on x% of product categories using the framework created in Q1 => 0/?, 0%](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/11206) 
    - [ ] Perform a competitor analysis for GitLab CI to understand the path forward. use the result to define the vision for the relevant category of CI with the PM

------------      
## Events to attend 
- [x] Rights Con
- [x] BAD Conference


### Deprioritized this quarter:
1. 
3. 

### Stop doing to make room
1. Values Illustration project
2. 

### Blogs to write
- [x] How to be more productive with GitLab CI

##  Books to read
- [x] Unlocking customer value chain
- [x] Hooked
- [x] The build trap
- [x] Microcopy


</details>
-------

<details><summary>FY 22 Q3</summary>

### Priority areas

- **Cross-stage collaboration:** Drive cross-stage collaboration by helping designers to identify dependencies and areas for cross-department work
- **Communication:** Set an example for how to effectively communicate across stages by frequently participating in asynchronous collaboration in issues and merge requests
- **Vision:** Exert significant influence on the overall objectives and long-range goals of your section by actively contributing to the Vision pages with Product and Engineering
- **Research:** Understand the nuances and considerations between problem and solution validation, and mentor other designers on how they plan research.
- Mentoring - 

[CI FY22-Q3 Priorities](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/45)
In this quarter I'll be aligning my personal KRs with verify Hive objectives for FY22-Q3

## Objectives

## 1. [Verify Hive Usability Shared Objective: FY22 Themes & SLOs of Bugs](https://gitlab.com/groups/gitlab-org/-/epics/6316)
- [ ] ~"group::pipeline execution" Deliver 2 ~security issues from Security aged list https://gitlab.com/gitlab-org/gitlab/-/issues/207988 and https://gitlab.com/gitlab-org/gitlab/-/issues/326216 => 0%

## 2. [Verify Hive High Performing Team Objective: MR Rate](https://gitlab.com/groups/gitlab-org/-/epics/6317)
- [ ] Update Pipeline Execution handbook page
- [ ] Update CI/CD UX page with the current practices
- [ ] Add the show/hide pattern for forms to Pajamas
- [ ] Refine the Pipeline Execution JTBDs and up level them

## 3. Stretch goal: [Verify Hive Deliver on the Golden Journey Team Objective: GMAU](https://gitlab.com/groups/gitlab-org/-/epics/6314)
### KR1: Get familiar with Package features and workflows
**Key Results:**
- [ ] Record a walkthrough of the Package related workflows and post it to [Share a common understanding of primary workflows for stage-groups within Verify](https://gitlab.com/gitlab-org/verify-stage/-/issues/90)

### KR2: Vision: Adoption-Find opportunities between package and CI combined for driving CI adoption
**Key Results:**
- [ ] Record a video and create an issue listing ideas of innovating within Testing and CI to drive adoption

## 4. Personal Objective
- [ ] Q3 focus: Develop business knowledge around Verify to prioritize strategic innovation (10% formal training, 20% mentorship, 70% on the job)
    - [ ] [70%] Participate in [What supports and fuels the SMB/MM pursuit of the Golden Journey?](https://gitlab.com/gitlab-org/ux-research/-/issues/1522)
    - [ ] [10%] Read `Measure what matters` and write a summary with learnings and watch [How Atlassian Uses Advanced Roadmaps](https://www.youtube.com/watch?v=fz7GthQSP0o&ab_channel=Atlassian)
    - [ ] [20%] Mentorship sessions with Jackie and 

## UX KRs
### [FY22-Q3 KR: Evaluate the experience of 10 product categories and make recommendations to improve learnability => 0/10, 0%](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/12084)
- [ ] Perform a heuristic evaluation around `Analyse CI data across projects` with Becka Lippert
- [ ] Assist Gina Doyle to perform a heuristic evaluation around `xyz` in testing

## Events to attend 
- [ ] CC Global Summit


### Deprioritized this quarter:
1. Issues related to `React to outcomes of automated tasks` JTBD
2. CI permissions mapping
3. 
4. 
5. 

### Stop doing to make room
1. [Strategic research: Simplify MWPS/MT/MTWPS with just auto-merge](https://gitlab.com/gitlab-org/gitlab/-/issues/325529) 
3. Application for UX events
2. Participation in All things Open
4. 

##  Books to read 
- [ ] Crossing the chasm
- [ ] Measure what matters
- [ ] (Optional) Super Thinking
- [ ] (Optional) Indistractable


</details>



