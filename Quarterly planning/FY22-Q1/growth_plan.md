# Veethika - FY22-Q1 Growth Plan

Milestones - 13.9 - 14.0


---------
``` 
Overarching objective: Improve subject matter expertise to facilitate cross stage collaboration 
```

# Personal KRs
## KR1: 🧭 Strengthen the research foundation for CI

**Key Results:**
- [x] [Create a general research project for CI for a holistic picture](https://dovetailapp.com/projects/be7bcb1f-9fdf-42a7-907d-d10972da0845/readme)
- [x] Create a mechanism to source data from across projects into the general project
- [x] Map E2E JTBDs for CI
- [x] Document learnings in a video
- [x] Make necessary updates to the handbook
  - [ ] MR1: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/76931
  - [ ] MR2: 
- [x] Conduct solution validation for Pipeline Overview redesign proposal
- [x] Evaluate maturity of GitLab CI
------

## KR2: 🎓 Help other UX team members understand complex features provided as a part of the GitLab CI offering
**Objective-** CI has a lot of complex features. To boost cross-stage collaboration, gain knowledge of one of the core areas within CI and help other team members to understand those features by simplifying them.
**Key Results:**
- [ ] learn more about CI Variables and their usage
     - [ ] Educate yourself further on CI/CD Variables([LinkedIn Video](https://www.linkedin.com/learning/continuous-delivery-with-gitlab/environments?u=2255073)) 
     - [ ] Write a blog about CI variable(since that's one of the two areas of focus for CI for this quarter)

-------

## KR3: 🤝 Partner with other teams to enhance the experience for other overlapping workflows with CI. Evangelise the sections in CI that could be leveraged by other stage-groups
**Objective-** Verify::CI has overlaps with workflows outside the Verify stage as well. Understand the overlaps and define the next iterations for them in collaboration.
**Key Results:**
- [x] Discuss possibilities with team members from other stage groups
  - [x] Discussion with @nadiasotnikova - How can we increase the SUS for the overall CI+PA groups?
  - [x] Discussion with @pedroms - merge request widget
- [ ] Document/ create issues for collaborative plans
  - [ ] ...
  - [ ] ...

--------

## KR4: 📚 Enhance knowledge around the subject matter(stage-group) as well as design management for better efficiency
- [ ] Take 2 courses on LinkedIn learning on CI/CD practices and tools
     - [ ] TBD
     - [ ] TBD
- [ ] Design research: Try new methodologies from https://researchops.community/resources/
     - [ ] Share your learnings from the process


---------
---------
# Organizational KRs

## KR1: 🦾 Stay true to our "Everyone can contribute" ethos by improving our knowledge of accessibility standards throughout Product Design
**Objective-** Make our UX department a "best place to work" for designers, researchers, and technical writers.
**Key Results:**
- [ ] Complete the [Accessibility for Web Design LinkedIn training](https://www.linkedin.com/learning/accessibility-for-web-design/welcome?u=2255073)

------
## KR2: ⚡️ Understand the impact of our SUS-related changes more quickly by piloting a process for iterative testing
**Objective-** (Great Product): Increase our SUS score to 72.5 or higher through UX design improvements
**Key Results:**




------
## KR3: 🔬 Encourage adoption, reduce friction, and drive cross-stage SPO by understanding how customers are leveraging multiple stages
**Objective-** Help our R&D org target the most meaningful SUS improvements
**Key Results:**
- [x] 3 issues identified



-------
## KR4: 🔬 Create passionate user advocates by giving the entire company better exposure to the valuable insights we learn in user research
**Objective-** Help our R&D org target the most meaningful SUS improvements
- [ ] Broadcast the insights from solution validation research and CMS research to the relevant teams in the org, even outide iterations
- [ ] Create a format for sharing validated JTBDs and insight with the larger design community 


--------

## Events to attend as a speaker
- [ ] Techknowday - March 8th | Topic: dark defaults
- [ ] ...
- [ ] ...

``` 
Deprioritized this quarter:
1. [Collaborative critique session to improve CI/CD settings page](https://gitlab.com/groups/gitlab-org/-/epics/4813)
2. --
```

##  Books to read
- [ ] Super Thinking
- [ ] Hooked
- [ ] Play like a feminist

