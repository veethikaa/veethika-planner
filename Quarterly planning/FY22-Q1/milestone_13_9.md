# Milestone 13.9 From 18th feb to 17th March
Milestone Planning Issue: 

```
⚡️ Highlights
• Kick started CMS research + Solution validation for pipeline overview page
• 
```

## 🎳 Progress with Issues

- [ ]  https://gitlab.com/gitlab-org/gitlab/-/issues/277391
- [ ]  https://gitlab.com/gitlab-org/gitlab/-/issues/15337
- [ ]  https://gitlab.com/gitlab-org/gitlab/-/issues/225488
- [ ]  https://gitlab.com/gitlab-org/gitlab/-/issues/225488

## 👁 Progress with Research

- [ ] https://gitlab.com/gitlab-org/ci-cd/continuous-integration/-/issues/52
- [ ] https://gitlab.com/gitlab-org/ux-research/-/issues/1292
- [ ] https://gitlab.com/gitlab-org/gitlab/-/issues/300804
- [ ] Open a dovetail project and import the two project mentioned by Thao from Chorus.ai
- [ ] Recruit users for Solution Validation
- [ ] Recruit DevOps for CMS Research

## 🔗 Cross-team engagements

- [ ] https://gitlab.com/gitlab-org/gitlab/-/issues/300549
- [ ] 
- [ ] 

## 💪 Personal goals

- [x] Improve merge train epic
- [ ] Finish blog on Dark Defaults
- [ ] Apply for two conferences
- [ ] Coffee chat me three team-members

