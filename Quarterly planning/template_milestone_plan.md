
# Milestone XX.X From <Month_day> to <Month_day>
Milestone Planning Issue: 

```
⚡️ Highlights
• 
• 
```

## 🎳 Progress with Issues

- [ ]  
- [ ]  
- [ ]


## 👁 Progress with Research

- [ ] 
- [ ] 
- [ ] 

## 🔗 Cross-team engagements

- [ ] 
- [ ] 
- [ ] 

## 💪 Personal goals

- [ ] 
- [ ] 
- [ ] 

