# FY21 Q4 From October 1, 2020 to January 31, 2021

```
⚡️ Highlights
• CI AMA Session presentation
• 1 year vision for pipeline overview page 
• Merge Trains Explained - Blog
```

## 🎳 Progress with [UX OKRs](https://gitlab.com/gitlab-org/verify-and-release-ux-team/-/issues/22)
 
- [x]  [Improve GitLab performance, quality, and velocity by making GitLab development](https://gitlab.com/groups/gitlab-com/-/epics/1043)
  - [x] https://gitlab.com/gitlab-org/gitlab/-/issues/242024 - ✅
  - [x] https://gitlab.com/gitlab-org/gitlab/-/issues/242025 - ✅
  - [-] https://gitlab.com/gitlab-org/gitlab/-/issues/242033 - No bs-callout class found
  - [x] https://gitlab.com/gitlab-org/gitlab/-/issues/227256 - ✅
  - [x] https://gitlab.com/gitlab-org/gitlab/-/issues/229669 - ✅

-----

- [x]  [Drive usage by improving usability](https://gitlab.com/groups/gitlab-com/-/epics/1045)
  - [x] [Identify items to apply feature::enhancement label to](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Acontinuous%20integration&label_name[]=feature%3A%3Aenhancement)
  - [x] [Verify CI JTBDs](https://about.gitlab.com/handbook/engineering/development/ops/verify/continuous-integration/jtbd/)
   
   --------
  - [x] [Make GitLab Settings a great experience for our users](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/9467)
    - [x] [Design Crit Session for CI/CD Settings](https://gitlab.com/gitlab-org/ux-research/-/issues/1195) - Kickstarted, Analysis awaited

-----

- [ ]  [Help product development move faster and more efficiently through better design iteration](https://gitlab.com/groups/gitlab-com/-/epics/1044)
  - [x] [Complete DIB training](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/9460)
  - [x] [Complete Performance Review](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/9323)
  - [x] [Complete Performance Review](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/9323)
  - [x] [Dogfood iterations feature](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/9322)
    - [x] https://gitlab.com/gitlab-org/gitlab/-/issues/280772 
    - [x] https://gitlab.com/gitlab-org/gitlab/-/issues/207988 
    - [x] https://gitlab.com/gitlab-org/gitlab/-/issues/277391

-----


## 💪 Progress with personal OKRs

### Get a better knowlede of categories within CI and organize help the efforts 
-------
- [x] Organize the Verify::Continuous Integration work better
  - [x] [Vision epics](https://gitlab.com/groups/gitlab-org/-/epics/4794)
  - [x] [Merge Train Epic] - Created
- [x] Understand the working of Merge train - Blog - [How Merge Trains Work](https://about.gitlab.com/blog/2020/12/14/merge-trains-explained/)
- [x] Created [Continuous Integration UX page](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/ci-cd/continuous-integration/)
- [x] Created [Continuous Integration JTBD page](https://about.gitlab.com/handbook/engineering/development/ops/verify/continuous-integration/jtbd/) 

### Engage with designers from other stage group and explore cross stage collaboration

- [x] Coffee chats with UX team mates
  - [x] Holly Reynolds
  - [x] Maria Vrachni
  - [x] Sunjung
  - [x] Nick Post
  - [x] Camellia X YANG

### Learn More about GitLab Values:
- [x] Started a new project to illustrate GitLab Values and share more information around them through twitter: https://gitlab.com/v_mishra/gitlab-values-illustrations. Twitter impressions: https://twitter.com/veethikaa/status/1353972253088600064 https://twitter.com/veethikaa/status/1355953129129295874

--------

## ⛈ New things I learnt about GitLab

- [x] How to spin up a cluster without leaving GitLab and GitLab agents - Maria Vrachni
- [x] How to edit APIs using Postman App - Thao
- [x] Setting up gdk.test - Miguel Rincon 

## Other Activities:
- Added a section to hanbook about handling data collected from social media for research purpose- https://about.gitlab.com/handbook/engineering/ux/ux-research-training/collecting-useful-data/#using-data-from-social-media-engagements)
- Updated the [Product Principles page ](https://about.gitlab.com/handbook/product/product-principles/) to include an exception example for the section `Avoid instance wide features` - https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/71332

