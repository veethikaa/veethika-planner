


# Milestone 13.7 From November 18 to December 18
Milestone Planning Issue: [Issue](https://gitlab.com/gitlab-org/ci-cd/continuous-integration/-/issues/10)

```
⚡️ Highlights
• Proposed a monthly ritual for promoting the most impactful release from a milestone(in the planning issue)
• [Added a section to hanbook about handling data collected from social media for research purpose](https://about.gitlab.com/handbook/engineering/ux/ux-research-training/collecting-useful-data/#using-data-from-social-media-engagements)
• 
•
```

## 🎳 Progress with Issues

- [ ]  [Prep for GitLab Virtual Meetup - AMA with GitLab CI team](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/-/issues/1126)
- [ ]  [Update project settings copy for merge trains support for fast forward merge](https://gitlab.com/gitlab-org/gitlab/-/issues/282422)
- [ ]  [Add system messages to MR page for merge trains support for fast forward merge](Add system messages to MR page for merge trains support for fast forward merge)
- [ ]  [Job ignores previous stage if cancelled and retried](https://gitlab.com/gitlab-org/gitlab/-/issues/207988)



## 👁 Progress with Research

- [ ] [Understand the key metrics useful for analyzing and optimising a pipeline(problem validation](https://dovetailapp.com/projects/2d7359de-3d5b-4b0d-8e05-6b0c4c33016f/readme)
- [ ] [Design critique session - Evaluate Settings >> CI/CD page](https://gitlab.com/gitlab-org/ux-research/-/issues/1195)
- [ ] [Understand documentation accessibility pain-points for CI users](https://dovetailapp.com/projects/8bf54f59-46bd-4790-9e2b-bc777dd4b861/readme)


## 🔗 Cross-team engagements

- [ ] [Design critique session - Evaluate Settings >> CI/CD page](https://gitlab.com/gitlab-org/ux-research/-/issues/1195)
- [ ] 
- [ ] 

## 💪 Personal goals

- [ ] 
- [ ] 
- [ ] 

