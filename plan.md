# 🥅 Veethika's Growth Plan
- This is a living documentation of my progress and growth as a Senior Product Designer at GitLab 🦊 
- The documented objective relates to the responsibilities of Senior Product Designer as well as Staff Product Designer role at GitLab
- Each financial year has an personal over-arching goal that is further broken down into quarterly personal OKRs

```
📰 Latest: 


```

-----------

## 🔍 What are you looking for?
This repository has been organised in the following order:
> Plan - Showcases the overarching annual plan and progress
>> Quarterly Plan - Detailed breakdown of the annual plan into quarterly activities
>>> Milestone Plan - To document the milestone specific tasks

--------

## 🌟 FY22 - From February 1, 2021 to January 31, 2022

```
Overarching objectives[TBD]: Grow in the role as an independant designer for Continuous Integration with

```

[Growth Plan for FY22Q1 - - From February 1, 2021 to April 30, 2021](https://gitlab.com/v_mishra/veethika-planner/-/blob/master/Quarterly%20planning/FY22-Q1/growth_plan.md)



## Important epics(in the order of priority):
[1 Year Focus - Verify::CI](https://gitlab.com/groups/gitlab-org/-/epics/4794)

[Verify CI research initiatives](https://gitlab.com/groups/gitlab-org/-/epics/4927)

[MR Widget Improvements for CI](https://gitlab.com/groups/gitlab-org/-/epics/4926)

[3 Years Vision for Verify::CI](https://gitlab.com/groups/gitlab-org/-/epics/4793)

[3 Years Focus - Verify::CI](https://gitlab.com/groups/gitlab-org/-/epics/4898)

[Merge train improvements](https://gitlab.com/groups/gitlab-org/-/epics/5122)


