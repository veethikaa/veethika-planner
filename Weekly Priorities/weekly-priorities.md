# Veethika's weekly priorities
`This document is to communicate my weekly priorities. Reviewing MRs wouldn't be listed as items in the list as the assignment isn't planned but they're of utmost priority. [See Product designer priorities.](https://about.gitlab.com/handbook/engineering/ux/product-designer/#priorities)`

## 🚨 Upcoming long time off: August 6th - August 11 🚨


## Weekly capacity estimate

------ 

### 🪀 _Week of 2024-11-25 - 2024-11-29 
- [ ] 1. 
- [ ] 2. 
- [ ] 3.  
- [ ] 4. 
- [ ] 5. 

### Week of 2024-11-18 - 2024-11-22 
- [ ] 1. 
- [ ] 2. 
- [ ] 3.  
- [ ] 4. 
- [ ] 5. 

### Week of 2024-11-11 - 2024-11-15 
- [ ] 1. 
- [ ] 2. 
- [ ] 3.  
- [ ] 4. 
- [ ] 5. 

### Week of 2024-11-04 - 2024-11-08 
- [ ] 1. 
- [ ] 2. 
- [ ] 3.  
- [ ] 4. 
- [ ] 5. 


### Week of 2024-10-28 - 2024-11-01 
- [ ] 1. 
- [ ] 2. 
- [ ] 3.  
- [ ] 4. 
- [ ] 5. 

### Week of 2024-10-21 - 2024-10-25 
- [ ] 1. 
- [ ] 2. 
- [ ] 3.  
- [ ] 4. 
- [ ] 5. 

### Week of 2024-10-14 - 2024-10-18 
- [ ] 1. 
- [ ] 2. 
- [ ] 3.  
- [ ] 4. 
- [ ] 5. 

### Week of 2024-10-07 - 2024-10-11 
- [ ] 1. MR reviews - community contribution
- [ ] 2. PCSAT interviews
- [ ] 3. Instance level compute Quota 
- [ ] 4. Write a guide for usability hackathon for KR

### Week of 2024-09-30 - 2024-10-04 
- [ ] 1. Organize PCSAT follow up interviews
- [ ] 2. Troubleshoot trigger from other locations
- [ ] 3. Usability goals - analyse responses

### Week of 2024-09-23 - 2024-09-27 
- [ ] 1. Discuss the requirements for secure app dev workflow + Organize sync meetings
- [ ] 2. [Build failure type dashboar request](https://gitlab.com/gitlab-data/product-analytics/-/issues/2174#note_2122771280)
- [ ] 3. RCA feedback mechanism
- [ ] 4. Link to navigate to CI/CD analytics from the pipeline list view

### Week of 2024-09-16 - 2024-09-20 
- [ ] 1. Identify opportunity for `Add links for GL university`
- [ ] 2. [Feedback mechanism for RCA](https://gitlab.com/gitlab-org/gitlab/-/issues/461667#note_2115375170)
- [ ] 3. No notification for failed scheduled pipeline + [view schedule config](https://gitlab.com/gitlab-org/gitlab/-/issues/404403#note_2117590446)
- [ ] 4. Feedback on CI category maturity
- [ ] 5. Discuss the requirements for secure app dev workflow

### Week of 2024-09-09 - 2024-09-13 [Time off]

### Week of 2024-09-02 - 2024-09-06 
- [ ] 1. Community MR reviews
- [ ] 2. RCA feedback mechanism
- [ ] 3. Doc on CI/CD usability week 
- [ ] 4. Prep for next week's time off
- [ ] 5. 

### Week of 2024-08-26 - 2024-08-30 
- [ ] 1. Duo workflow analysis
- [ ] 2. RCA feedback mechanism
- [ ] 3. Follow up on beautifying of our UI and deferred UX 
- [ ] 4. Doc on Usability week
- [ ] 5. 

### Week of 2024-08-12 - 2024-08-16 
- [ ] 1. Usability week participation
- [ ] 2. CI usability issues for hackathon - Office hours
- [ ] 3. Duo workflow discussions 
- [ ] 4.
- [ ] 5. 

### Week of 2024-08-05 - 2024-08-09 
- [ ] 1. RCA discussion
- [ ] 2. Beautifying of our UI - share survey
- [ ] 3. Discussion on research direction for Ultimate ARR+ 
- [ ] 4. Socialize RCA vision epic
- [ ] 5. 

### Week of 2024-07-29 - 2024-08-02 
- [ ] 1. RCA vision - Create issues and epics for themes
- [ ] 2. RCA - discussion with AI framework team/Jacki/Nicolle
- [ ] 3. Prep for usability week 
- [ ] 4. 
- [ ] 5. 

### Week of 2024-07-22 - 2024-07-26 
- [ ] 1. Duo workflow analysis
- [ ] 2. RCA long term vision
- [ ] 3. Follow up on beautifying of our UI and deferred UX 
- [ ] 4. Propose a blog post on RCA(UX)
- [ ] 5. 

### Week of 2024-07-18 - 2024-07-19 
- [ ] 1. Usability test analysis
- [ ] 2. Get reviews for blog post on merge trains, do pending MR reviews for the feature
- [ ] 3. Prepare a new plan for deferred UX 
- [ ] 4. 
- [ ] 5. 

### Week of 2024-07-08 - 2024-07-12 
- [ ] 1. Usability test and Duo workflows interviews
- [ ] 2. Create short term vision(before GA for RCA)
- [ ] 3. Prepare to define the vision for RCA
- [ ] 4. 
- [ ] 5. 

### Week of 2024-07-01 - 2024-07-05 
- [ ] 1. Usability test and Duo workflows interviews
- [ ] 2. Duo workflow sync with Pedro and Erika
- [ ] 3. Review RCA MRs
- [ ] 4. 
- [ ] 5. 

### Week of 2024-06-24 - 2024-06-28 
- [ ] 1. Merge train viz MR reviews
- [ ] 2. RCA usability test recruitment follow up and interviews
- [ ] 3. Create a re-vamp proposal for Beautifying of our UI


### Week of 2024-06-17 - 2024-06-21 
- [ ] 1. Recruit for usability study for RCA
- [ ] 2. Review RCA MRs
- [ ] 3. Complete the discussion guide and have it reviewed
- [ ] 4. Mid year check-in

### Week of 2024-06-10 - 2024-06-14 
- [ ] 1. Wrap up dependency firewall prototypre and handoffs
- [ ] 2. Get started on usability test for RCA - Create issues and get the requirements straight
- [ ] 3. Sync with ux-ai-integration and frameworks teams to decide on methods.
- [ ] 4. Growth and development plan

### Week of 2024-06-03 - 2024-06-07 
- [ ] 1. Customer interviews for dependency firewall
- [ ] 3. PE MR reviews
- [ ] 4. Create illustration for dependency firewall

### Week of 2024-05-27 - 2024-05-31 
- [ ] 1. Create research issues and start recruiting for dependency firewall validation
- [ ] 2. Polish teh prototype
- [ ] 3. PE MR reviews
- [ ] 4. Beautifying of our UI reviews

### Week of 2024-05-20 - 2024-05-24 
- [ ] 1. Discuss dependency firewall with team members and partnering designers
- [ ] 2. Make changes to design
- [ ] 3. PE MR reviews
- [ ] 4. Create branch: Illustration for merge trains

### Week of 2024-05-13 - 2024-05-17 
- [ ] 1. Analyse the Dependency firewall research
- [ ] 2. Create prototype for dependency firewall
- [ ] 3. PE MR reviews

### Week of 2024-05-06 - 2024-05-10 
- [ ] 1. RCA: UX heuristics 
- [ ] 2. Create recommendations
- [ ] 3. PE MR reviews

### Week of 2024-04-29 - 2024-05-03 
- [ ] 1. Catch up after time off
- [ ] 2. Review community MRs

### Week of 2024-04-22 - 2024-04-26 ----TIME OFF  
- [x] 1. Prepare for coverage

### Week of 2024-04-15 - 2024-04-19 
- [x] 1. Lay down the research project structure and user stories
- [x] 2. Watch user interviews for package and set up GDK
- [x] 3. PE MR review
- [ ] 4. Create branch: Illustration for merge trains

### Week of 2024-04-08 - 2024-04-12 
- [x] 1. Prepare for Package - Level Up courses + Previous interviews on Dovetail
- [x] 2. Complete UX OKR work
- [x] 3. MR review
- [x] 4. Illustration for merge trains


### eek of 2024-04-01 - 2024-04-24 
- [x] 1. Finalize proposals for BofUI + PE issues
- [x] 2. UX OKR
- [x] 3. Dependency firewall - interviews 
- [x] 4. 

### Week of 2024-03-25 - 2024-03-28 
- [x] 1. AI work shop
- [x] 2. Add proposals for BofUI 17.1
- [x] 3. Add proposals for PE issues
- [x] 4. Plan for borrow request

### Upcoming:Week of 2024-03-18 - 2024-03-22 
- [x] 1. Evaluate if root cause analysis can be moved to Duo
- [x] 2. Actionable Insights Workshop: Conversational pairworking with AI
- [x] 3. 16.11 design issues
- [x] 4. Plan for beautifying of our UI 17.1

## Week of 2024-03-11 - 2024-03-15 🎉 S U M M I T 🎉 

### Week of 2024-03-04 - 2024-03-08 
- [ ] 1. IGP
- [ ] 2. Merge train visualization
- [ ] 3. UX OKRs
- [ ] 4. 

### Week of 2024-02-26 - 2024-03-01 
- [ ] 1. Discussion with Code-review
- [ ] 2. Prep for summit
- [ ] 3. Merge train visualization
- [ ] 4. design mentorship + IGP discussions

### Week of 2024-02-19 - 2024-02-23 
- [ ] 1. Individual growth plan discussion
- [ ] 2. Job logs beyond 512KiB
- [ ] 3. Final variables blog review follow-ups, apply for ATO
- [ ] 4. Merge train visualization

### Week of 2024-02-12 - 2024-02-16 
- [ ] 1. Catch up on to-do items
- [ ] 2. Close remaining design issues for 16.9
- [ ] 3. Work on growth plan/proposal
- [ ] 4. Start with the validation research planning for batch merge

### Week of 2024-02-05 - 2024-02-09 
- [ ] 1. catch up with Staff designers
- [ ] 2. Execute on 16.9 plan for pipeline execution
- [ ] 3. 16.9 issues, 16.10 planning
- [ ] 4. Create growth and development plan

### Upcoming:Week of 2024-01-29 - 2024-02-02 
- [ ] 1. Rearrange merge train epics
- [ ] 2. Discuss growth and development plan with Rayana
- [ ] 3. Analyse and share findings from research
- [ ] 4. Craete issues for research findings

### Week of 2024-01-22 - 2024-01-26 
- [ ] 1. Beautifying of our UI - variables blog
- [ ] 2. [Competitor evaluation MT](https://gitlab.com/gitlab-org/ux-research/-/issues/2765)
- [ ] 3. Analysis of competitor evaluation + Merge experience research
- [ ] 4. organize merge train epics and record vision
- [ ] 5. TUFTS slack channel and project

### Week of 2024-01-15 - 2024-01-19 
- [ ] 1. Merge train research - interviews
- [ ] 2. [Support MR search scope for pipelines](https://gitlab.com/gitlab-org/gitlab/-/issues/336402)
- [ ] 3. Refine the [FF merge support for MT](https://gitlab.com/groups/gitlab-org/-/epics/4911) epic
- [ ] 4. create a fresh proposal for merge train visualization

### Week of 2024-01-08 - 2024-01-12 
- [ ] 1. Implement https://gitlab.com/gitlab-org/gitlab/-/issues/434668#note_1717652300 
- [ ] 2. Help Bonnie with CI_JOB_TOKEN work
- [ ] 3. With source code: https://gitlab.com/gitlab-org/gitlab/-/issues/344113#note_1718198192 
- [ ] 4. Sync with Mireya on variables : https://gitlab.com/gitlab-org/gitlab/-/issues/418331

### Week of 2024-01-01 - 2024-01-05 (partially available)
- [ ] 1. Catch up on to-do items from time off
- [ ] 2. 


---------HNY-2024---------

### Upcoming:Week of 2023-12-25 - 2023-12-29 
- [ ] 1. Time off: Coverage issue https://gitlab.com/gitlab-org/gitlab-design/-/issues/2342 


### Upcoming:Week of 2023-12-18 - 2023-12- 
- [ ] 1. [PE issues from the list for 16.8](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/92)
- [ ] 2. User-interviews!!
- [ ] 3. Have Bonnie shadow on variables related issues
- [ ] 4. Beautifying of our UI
- [ ] 5. Tufts summary writing for projects

### Week of 2023-12-11 - 2023-12- 
- [ ] 1. Highest priority: https://gitlab.com/gitlab-org/gitlab/-/issues/416619
- [ ] 2. Beautifying of our UI final follow-ups with Mireya
- [ ] 3. Set-up UX Scorecard exercise for Bonnie
- [ ] 4. Invite participants to user-interview for mergability experience and finalize the discussion guide.

### Week of 2023-12-04 - 2023-12-08
- [x] 1. Create MR for MR report emty state
- [x] 2. Complete design for two issues from 16.7 - https://gitlab.com/gitlab-org/gitlab/-/issues/19594 
- [x] 3. Setup the discussion guide for upcoming research and schedule interviews
- [x] 4. [Fireside chat/Ownpath](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/8585) 
- [ ] 5. 

### Week of 2023-11-27 - 2023-12-01
- [x] 1. Sync with BE team members to work out a plan for reasearch scenarios - [Issue](https://gitlab.com/gitlab-org/ux-research/-/issues/2764) - 3 meetings
- [x] 2. Collaborate with Foundations to document empty state for merge report widget
- [x] 3. Check the status of the issues for [Beautifying of our UI 16.8](https://gitlab.com/gitlab-org/gitlab/-/issues/418331)
- [x] 4. Complete MR reviews

### Week of 2023-11-20 - 2023-11-24 
- [x] 1. Create issues to change icons where pipeline icons are wrongly used to show status
- [x] 2. Prep for Bonnie's onboarding, design a plan
- [x] 3. Set up new processes with Rutvik(PM) for PE design work
- [x] 4. 16.7 planning 
- [x] 5. Make progress on the set up for merge experience research

### Week of 2023-11-13 - 2023-11-17 
- [x] 1. Follow up on the pipeline icon vs status icon documentattion
- [x] 2. Follow up on teh assigned MR reviews
- [x] 3. Map the empty states in PE and model a process for team members to follow
- [x] 4. Fix jet lag 😞

### Week of 2023-11-06 - 2023-11-10 
- [x] 1. Merge experience research - define scenarios to test
- [x] 2. 16.6 design issues https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/90
- [x] 3. Map empty states for pipelines 
- [x] 4. Collaborate with Gina on Tufts engagement

### Week of 2023-10-30 - 2023-11-03 
- [x] 1. Set-up for the merge experience research - start collaboration with code -review
- [x] 2. 16.6 design issues https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/90
- [x] 3. Discuss the plan and lay down first steps for https://gitlab.com/gitlab-org/gitlab/-/issues/425861
- [x] 4. Kickstart the discussion on usage of pipeline icons


### Week of 2023-10-23 - 2023-10-27 
- [x] 1. Complete talent assesment
- [x] 2. Lay down the plan for Merge experience research
- [x] 3. Deliver 16.5 issues that're stuck in design workflow phase
- [x] 4. Present in product showcase

### Upcoming:Week of 2023-10-16 - 2023-10-20 
- [x] 1. Complete 100% transition to PE 
- [x] 2. Finish Assignment 2 analysis and make changes to Figma file, broadcast
- [x] 3. Align on the MR widget empty state 
- [x] 4. Prepare for merge train research 
- [x] 5. Plan for beautifying of our UI 16.8

### Week of 2023-10-08 - 2023-10-13 
- [x] 1. Wrap secrets assignment 2 analysis
- [x] 2. PE issues: https://gitlab.com/gitlab-org/gitlab/-/issues/425062 https://gitlab.com/gitlab-org/gitlab/-/issues/423922
- [x] 3. unblock team members on 16.5 implementation issues
- [x] 4. Merge train research- follow up on plan with PM

### Week of 2023-10-03 - 2023-10-07 
- [x] 1. Catch up on to-dos
- [x] 2. Secret management assignment 2 responses - Analyse
- [x] 3. Pipeline execution issues - https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/89

### Week of 2023-09-18 - 2023-09-22 
- [x] 1. Deliver designs for growth issues
- [x] 2. UX Showcase on secrets
- [x] 3. Secrets assignment 2 - send out the link
- [x] 4. Create a qualtrics based form for CI_JOB_TOKEN for a customer
- [x] 5. Start the prep for Mereg Train competitor evaluation
- [x] 6. Add details to [coverage](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2342) and planning issues 


### Week of 2023-09-11 - 2023-09-15 
- [x] 1. [In-product prompt for CI Adoption if Jenkins file is detected](https://gitlab.com/gitlab-org/gitlab/-/issues/423489)
- [x] 2. Organize the design issue for secrets MVC with all the background info - [Issue](https://gitlab.com/gitlab-org/gitlab/-/issues/419544)
- [x] 3. Move ahead with the drawer buttons guidelines
- [x] 4. Freeze the plan for 16.5
- [x] 5. Finish recording the presentation for CC Summit

### Upcoming:Week of 2023-09-05 - 2023-09-08 
- [x] 1. Create issues for nav changes for secrets and artifacts https://gitlab.com/gitlab-org/gitlab/-/issues/424812
- [x] 2. 3 User interviews for secrets
- [x] 3. Follow-up on recruitment for CI_JOB_TOKENs
- [ ] 4. Create a qualtrics based form for CI_JOB_TOKEN for a customer
- [x] 5. Complete benchmarking issues

### Week of 2023-08-28 - 2023-09-01 
- [x] 1. Prepare discussion guide + Recruit for CI_JOB_TOKENs research https://gitlab.com/gitlab-org/ux-research/-/issues/2614
- [x] 2. Invite Participants from secrets research to understand the flow better.
- [x] 3. Help team members decide on blog/CFP topic https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/85 
- [x] 4. Setup transition issue for PS to PE: https://gitlab.com/gitlab-org/gitlab-design/-/issues/2347 
- [x] 5. Add guidance for drawer action buttons https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/merge_requests/3497
- [ ] 6. Create issues for nav changes for secrets and artifacts

### Week of 2023-08-21 - 2023-08-24 
- [ ] 1. 15%: UX Showcase - iterations + Secrets
- [x] 2. 45%: Benchmarking issues - Collaborate with Code review on https://gitlab.com/gitlab-org/gitlab/-/issues/422072 and https://gitlab.com/gitlab-org/gitlab/-/issues/404685 - This issue has been deprioritised
- [x] 3. 25%: Prepare discussion guide + Recruit for CI_JOB_TOKENs research https://gitlab.com/gitlab-org/ux-research/-/issues/2614
- [x] 4. 5%: Prepare coverage issue for upcoming time off 
- [x] 5. 10%: Invite CI/CD UX team members to https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/85

### Week of 2023-08-14 - 2023-08-18 
- [ ] 1. 40%%: Action items from [early adopters feedback](https://gitlab.com/gitlab-org/ux-research/-/issues/2470#note_1505367117)
- [x] 2. 30%: Solution validation for CI_JOB_TOKEN(recruit and put together the discussion guide)
- [x] 3. 10%: Help team with public presence
- [x] 4. 20%: Finalise 16.4 design issues

### Week of 2023-08-xx - 2023-06-xx 
- [x] 1. Look at the feedback from the survey and organize next steps [link](https://gitlab.eu.qualtrics.com/responses/#/surveys/SV_b9OhRHy9hF63flc)
- [x] 2. Finalize proposals for 
     - [x] 1. [Add a warning to the add variable modal for potential content leaking](https://gitlab.com/gitlab-org/gitlab/-/issues/415076)
     - [x] 2. [Better UX for new manual variable inputs](https://gitlab.com/gitlab-org/gitlab/-/issues/377271)
- [ ] 3. Create navigation discussion issues for
     - [ ] Removing artifacts
     - [ ] Moving secrets and variables

### Week of 2023-07-31 - 2023-08-04 
- [x] 1. Add proposal for: [Add a warning to the add variable modal for potential content leaking](https://gitlab.com/gitlab-org/gitlab/-/issues/415076)
- [x] 2. Add proposal for: [Better UX for new manual variable inputs](https://gitlab.com/gitlab-org/gitlab/-/issues/377271)
- [x] 3. Discussion with AppSec
- [x] 4. Send out prototype link to early adopters for secrets
- [x] 5. Plan for 16.4

### Week of 2023-07-24 - 2023-07-28 
- [x] 1. Secrets POC discussions
- [x] 2. Organize the task for early adopters program
- [x] 3. GitLab interview training

### Week of 2023-07-17 - 2023-07-21 
- [x] 1. Focus on delivering the first draft of Secrets POC + taskflow map
- [x] 2. Organize the task for early adopters program
- [x] 3. Individual growth plan discussion

### Week of 2023-07-10 - 2023-07-14 
- [x] 1. Catch up on all to-do items + 16.3 kick off
- [x] 2. CI_JOB_TOKEN for groups
- [x] 3. Plan for Secrets Management POC and early adopter program
- [x] 4. Prepare for growth plan discussion
- [x] 5. Read about Augmentation vs Automation

-----------AWAY FROM 06-27 TO 07-07-------------

### Week of 2023-06-19 - 2023-06-22 
- [x] 1. [Beautifying UI 16.1 Blog](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/125809) ✨ -  
- [x] 2. Analyze [variables solution validation research](https://gitlab.dovetailapp.com/projects/3a8X9fB2D8KV8sIbbFeFKs/v/6bhFi8mNipdPPZ84DOy1wO)
- [x] 3. Sketching exercise - task 2: https://gitlab.com/gitlab-org/gitlab/-/work_items/413561 
- [x] 4. Follow up: Help with community contribution https://gitlab.com/gitlab-org/gitlab/-/merge_requests/121750
- [x] 5. Prepare coverage issue https://gitlab.com/gitlab-org/gitlab-design/-/issues/2307 

### Week of 2023-06-12 - 2023-06-16 
- [x] 1. [Beautifying UI 16.1 Blog](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/125809) ✨ -  
- [x] 2. User interviews - 10 particpants accepted
- [x] 3. Sketching exercise - coonclude task 1: Complete the scenarios
- [x] 2. Help with community contribution https://gitlab.com/gitlab-org/gitlab/-/merge_requests/121750


### Week of 2023-06-02 - 2023-06-09 
- [x] 1. [Beautifying UI 16.1](https://gitlab.com/gitlab-org/gitlab/-/issues/394768) ✨ -  
- [x] 2. Help with community contribution https://gitlab.com/gitlab-org/gitlab/-/merge_requests/121750
- [x] 3. Conduct user interviews for variables


### Week of 2023-05-29 - 2023-06-02 
- [ ] 1. [Beautifying UI 16.1](https://gitlab.com/gitlab-org/gitlab/-/issues/394768) ✨ -  
- [ ] 2. Sketching exercise for Secrets
- [ ] 3. Discussion on access control for variables
- [ ] 4. Keep the solution validation prototype ready
- [ ] 5. Check your issues weight on planning issues

### Week of 2023-05-22 - 2023-05-26 
- [x] 1. [Beautifying UI 16.1](https://gitlab.com/gitlab-org/gitlab/-/issues/394768) ✨ -  
- [x] 2. Create an issue for sketching exercise
- [x] 3. Check on [recruiting for #2373](https://gitlab.com/gitlab-org/ux-research/-/issues/2478) and send out emails

### Week of 2023-05-15 - 2023-05-20 
- [x] 1. [Beautifying UI 16.1](https://gitlab.com/gitlab-org/gitlab/-/issues/394768) ✨ -  
- [x] 2. Analyse and share Competitive eval results https://gitlab.com/gitlab-org/competitor-evaluations/-/issues/37
- [x] 3. Check on [recruiting for #2373](https://gitlab.com/gitlab-org/ux-research/-/issues/2478)


### Week of 2023-05-08 - 2023-05-12 
- [x] 1. Revisit plan for 16.1 https://gitlab.com/gitlab-org/gitlab/-/issues/393932 and record Kick-off with Jocelyn 
- [x] 2. Start working on the competitive eval for secrets https://gitlab.com/gitlab-org/competitor-evaluations/-/issues/37
- [x] 3. Finish 3 design proposals in https://gitlab.com/groups/gitlab-org/-/epics/10506 for the KR
- [x] 4. [Beautifying UI 16.1](https://gitlab.com/gitlab-org/gitlab/-/issues/394768) ✨ - All issues design ready 

### Week of 2023-05-01 - 2023-05-05 
- [x] 1. Pipeline security vision JTBDs https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/123475 
- [x] 2. Lay-down the plan and create an epic for for Native secret Solution - https://gitlab.com/groups/gitlab-org/-/epics/10486
- [x] 3. Quick UI improvements for competive advantage - [Epic link](https://gitlab.com/groups/gitlab-org/-/epics/10506)
- [x] 4. Variables table redesign https://gitlab.com/gitlab-org/gitlab/-/issues/403176
- [x] 5. Plan research activities for secrets management 


###  Week of 2023-04-24 - 2023-04-28 
- [x] 1. Streamline processes and fix deliverables     
     - [x] Revisit JTBDs, Meeting formats, handbook, organize researches - [retro link](https://gitlab.com/gl-retrospectives/verify-stage/pipeline-security/-/issues/38#note_1369723923)
- [x] 2. [Beautifying UI 16.1](https://gitlab.com/gitlab-org/gitlab/-/issues/394768) - Start on jobs table layout 
- [x] 3. Start on [Improve the variables table layout to clearly represent the hierarchy](https://gitlab.com/gitlab-org/gitlab/-/issues/403176)
- [ ] ~~4. Create recruitment issues for solution validations~~ We're now doing a [continuous feedback research](https://gitlab.com/gitlab-org/ux-research/-/issues/2470) after discussion 

[April 3rd - 17th Time off, April 17 - 21 Catching up with To-dos and conversations because the universe changed its course while I was away 😄]

### Week of 2023-03-27 - 2023-04-02 
- [x] 1. Pipeline security vision workshop https://gitlab.com/gitlab-org/gitlab/-/issues/399295 
- [x] 2. Complete 15.11 issues  https://gitlab.com/gitlab-org/gitlab/-/issues/396594
- [x] 3. Set up the steps in UX roadmap workshop
- [ ] 4.  

###  Week of 2023-03-20 - 2023-03-24(F&F day) 
- [x] 1. UX roadmap prep https://gitlab.com/groups/gitlab-org/-/epics/10119  
- [x] 2. Get started on 15.11 issues https://gitlab.com/gitlab-org/gitlab/-/issues/396594 
- [x] 3. Growth and development: Watch UX roadmap videos 
- [x] 4. MR reviews 

###  Week of 2023-03-13 - 2023-03-17 
- [x] 1. UX Showcase - Secrets  
- [x] 2. Kick-off recording   
- [x] 3. Get started on 15.11 issues https://gitlab.com/gitlab-org/gitlab/-/issues/396594 
- [x] 4. Get started on a user story mapping for secrets 

### Week of 2023-06- - 2023-03-10 
- [x] 1.  Beautifying UI - finalise milestone and start [issue](https://gitlab.com/gitlab-org/gitlab/-/issues/394768) 
- [x] 2.  Planning with Pipeline security team 
- [x] 3.  Deprecate old handbook pages and set up new ones
- [x] 4.  Kick-off recording 


### Week of 2023-02-27 - 2023-03-03 
- [x] 1.  Coffee-chats
- [x] 2.  JTBDs transfer https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/120684
- [x] 3.  Learn: Secrets management and Variables
- [x] 4.  Publish the blog https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/120511 


###   Week of 2023-02-20 - 2023-02-24(2 days off) 
- [x] 1.  Work on [onboarding issue(https://gitlab.com/gitlab-org/gitlab-design/-/issues/2217)] for Pipeline Security
- [x] 2.  Set-up coffee chats with the new team members
- [x] 3.  Learn: Secrets management and Variables
- [x] 4.  Share the user-testing project for CI/CD minutes limit(projects) and a video summary of merge train findings with Jackie


### Week of 2023-02-13 - 2023-02-17 
- [x] 1.  Share [prototype](https://www.figma.com/proto/zdHrIFm9b4YSCnnuabYqsg/Add-CI%2FCD-quota-for-projects---Group?node-id=613%3A29706&scaling=min-zoom&page-id=603%3A24091&starting-point-node-id=603%3A32745&hotspot-hints=0) with users - Solution validation research https://gitlab.com/gitlab-org/ux-research/-/issues/2320
- [x] 2.  Organize Figma files for handover 😶‍🌫️
- [x] 3.  Merge train research wrap up
- [x] 4.  Wrap up: Support package team with https://gitlab.com/gitlab-org/gitlab/-/issues/388694 

### Week of 2023-02-06 - 2023-02-10 
- [ ] 1.  ~~Plan for 15.10, 15.11, 16.0 and 16.11~~  Attend AMAs
- [x] 2.  Prep: Solution validation research https://gitlab.com/gitlab-org/ux-research/-/issues/2320
- [ ] 3.  Blog post https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/14434 
- [x] 4.  Support package team with https://gitlab.com/gitlab-org/gitlab/-/issues/388694 

### Week of 2023-01-30 - 2023-02-03 
- [x] 1. Follow up on 15.9 issues 
- [x] 2. Get https://gitlab.com/gitlab-org/gitlab/-/issues/384874/ ready for contributions by moving on https://gitlab.com/gitlab-org/ux-research/-/issues/2320
- [x] 3. Blog post: https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/14434 
- [x] 4. Career development discussion https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/71

### Week of 2023-01-23 - 2023-01-27 
- [x] 1. Follow up on  - https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/merge_requests/3205#note_1233363907 
- [x] 2. Setup the projects for UX benchmarking with Caroline and Erika 
- [x] 3. 15.9 issues https://gitlab.com/gitlab-org/gitlab/-/issues/386188
- [x] 4. Check-in with Sunjung
- [ ] 5. Analyse merge train research 

### Week of 2023-01-16 - 2023-01-20 
- [x] 1. Follow up on  - Sunjung's onboarding - Plan for the onboarding task 
- [ ] 2. ~~Categorize SUS verbatims: https://gitlab.com/gitlab-org/ux-research/-/issues/2313~~ 
- [ ] 3. Setup the projects for UX benchmarking with Caroline and Erika
- [x] 4. Record Kick-off
- [x] 5.  Debugging a pipeline - close issue and define next steps Get clarity on the workflow for debugging a pipeline - https://gitlab.com/gitlab-org/ux-research/-/issues/2200


### Week of 2023-01-09 - 2023-01-12 
- [x] 1. Follow up on  - Sunjung's onboarding - Plan for the onboarding task 
- [ ] 2. Record Kick-off
- [x] 3. Follow up on benchmarking issues
- [x] 4. UX Showcase presentation https://gitlab.com/gitlab-org/gitlab-design/-/issues/2187 
- [ ] 5. Career development plan 


### Week of 2023-01-03 - 2023-01-06 
- [x] 1. Set-up issue for incubation - https://gitlab.com/groups/gitlab-org/-/epics/9541 
- [x] 2. Sunjung onboarding - Plan for the onboarding task
- [x] 3. Follow up on 15.8 issues https://gitlab.com/gitlab-org/gitlab/-/issues/386189
- [x] 4. Analyse results from research - Add custom names for pipelines
- [x] 5. Add comments to https://gitlab.com/gitlab-org/ux-research/-/issues/2281 
- [x] 6. UX Showcase presentation https://gitlab.com/gitlab-org/gitlab-design/-/issues/2187 


### Week of 2022-12-19 - 2022-12-23 (short week 🤒)
- [x] 1. Meet with darby to discuss incubation MR -  https://gitlab.com/gitlab-org/gitlab/-/merge_requests/100648 
- [x] 2. Series - Pipelines and more - outline details and make proposal https://gitlab.com/groups/gitlab-org/-/epics/9483
- [x] 3. Complete the changes in - blocked runner access support issue and the [chart for displaying shared runner usage](https://gitlab.com/gitlab-org/gitlab/-/issues/356076#note_1213221604)
- [x] 4. [Source-a-thon](https://gitlab.com/gitlab-com/people-group/talent-acquisition/-/issues/966)
- [ ] 5. Sunjung onboarding - share a walkthrough of JTBDs and the week 2 tasks
- [ ] 6. Complete the issues for 15.9

### Week of 2022-12-12 - 2022-12-16 
- [x] 1. Close [CI Scaling](https://gitlab.com/groups/gitlab-org/-/epics/8597) epic, record videos and outline the next steps 
- [ ] 2. Series - Pipelines and more - outline details and make proposal
- [x] 3. Finish 15.8 issues and keep them ready for development
- [x] 4. Plan for 15.9 
- [ ] 5. GDD: learn how to set up compliance


###  Week of 2022-12-05 - 2022-12-10 
- [x] 1. Follow up on UT test for job filters
- [ ] 2. Follow up on think big on secrets management
- [x] 3. UX buddy responsibilities
- [x] 4. Move forward 3 MRs from migration day
- [x] 5. Create new issue for project CI/CD limit and add designs



### Week of 2022-11-28 - 2022-12-02 
- [x] 1. Catch-up with notifications
- [x] 2. Set up the research plan on UT - https://gitlab.com/gitlab-org/ux-research/-/issues/1815
- [x] 3. Growth and development plan for Q4 + Watch [GitLab CI course on Udemy](https://www.udemy.com/course/gitlab-ci-pipelines-ci-cd-and-devops-for-beginners/)
- [x] 4. Discussion: [Changes to CI Permissions](https://gitlab.com/gitlab-org/gitlab/-/issues/382520) 

### Up next -  PTO 2022-11-13 - 2022-11-24 PT0  
- [x] 1. [Coverage issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2134)

###  Week of 2022-10-24 - 2022-10-28 
- [x] 1. Assess the feedback from https://gitlab.com/gitlab-org/gitlab/-/issues/363512/ and update https://gitlab.com/groups/gitlab-org/-/epics/5071#note_1102702111
- [x] 2. Create [an issue for feedback collected](https://gitlab.com/gitlab-org/ux-research/-/issues/2200) in the conference
- [x] 3. [Assign tasks in the coverage issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2134) for upcoming PTO
- [ ] 4. Design work for [Configure CI/CD quota for group projects](https://gitlab.com/gitlab-org/gitlab/-/issues/362287)

### Week of 2022-10-31 - 2022-11-04 - At All things open 2022 
- [x] 1. Present at ATO - It takes a community to craft an experience
- [x] 2. [Collect user feedback on top JTBDs at booth](https://gitlab.com/gitlab-org/ux-research/-/issues/2200) in the conference
- [x] 3. Make sure counterparts and teams are unblocked

### Week of 2022-10-24 - 2022-10-28 
- [ ] 1. Design work for [Configure CI/CD quota for group projects](https://gitlab.com/gitlab-org/gitlab/-/issues/362287)
- [x] 2. [Self assessment](https://docs.google.com/document/d/1khkrwgVpef1hkDmwuF8Mhz9BTrtZt4WaFAwjChDFRh4/edit)
- [x] 3. Create an epic for [Reduce number of steps for tasks](https://docs.google.com/spreadsheets/d/1FriC6J6Wwi4Fnpc8sJYIMOudc0MttyV66o_YIJowaZQ/edit#gid=699694143) - we're closing this out since there's no KR for SUS and recording a video
- [x] 4. Work on presentation for All things open 

### Week of 2022-10-17 - 2022-10-21 
- [ ] 1. Assess the feedback from https://gitlab.com/gitlab-org/gitlab/-/issues/363512/ and update https://gitlab.com/groups/gitlab-org/-/epics/5071#note_1102702111
- [x] 2. [Self assessment](https://docs.google.com/document/d/1khkrwgVpef1hkDmwuF8Mhz9BTrtZt4WaFAwjChDFRh4/edit)
- [x] 3. Define next steps for: [Mobile DevOps](https://gitlab.com/gitlab-org/gitlab/-/issues/378040) , Pipeline data tracking related activities , [pipeline icon](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/1416)
- [x] 4. [Configure CI/CD quota for projects under groups](https://gitlab.com/gitlab-org/gitlab/-/issues/362287) - begin discussions on a first draft
- [x] 5. MR Reviews: 1. | 2. | 3. 

### Week of 2022-10-10 - 2022-10-15 (canadian Thanksgiving + Hackathon ✨)
- [x] 1. [Assess the SUS and PNPS feedback](https://docs.google.com/spreadsheets/d/1FriC6J6Wwi4Fnpc8sJYIMOudc0MttyV66o_YIJowaZQ/edit#gid=699694143), align with JTBD and identify broader themes.
- [x] 2. [Self assessment](https://docs.google.com/document/d/1khkrwgVpef1hkDmwuF8Mhz9BTrtZt4WaFAwjChDFRh4/edit)
- [x] 3. Reformat weekly priorities
- [x] 4. Check if the designs [15.6 issues](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=opened&milestone_title=15.6&label_name%5B%5D=group%3A%3Apipeline%20execution&label_name%5B%5D=UX&first_page_size=20) are good to go
- [x] 5. Do a clean up of [design epic](https://gitlab.com/groups/gitlab-org/-/epics/5866) 
- [x] 6. Community MR Reviews: 1. | 2. | 3. | 4. | 5. 

### Week of 2022-10-03 - 2022-10-06 (Short week- F&F day + Visiting grant meet up at Montreal)
- [ ] 1. Assess the feedback from https://gitlab.com/gitlab-org/gitlab/-/issues/363512/ and update https://gitlab.com/groups/gitlab-org/-/epics/5071#note_1102702111 
- [ ] 2. Or, [Assess the SUS and PNPS feedback](https://docs.google.com/spreadsheets/d/1FriC6J6Wwi4Fnpc8sJYIMOudc0MttyV66o_YIJowaZQ/edit#gid=699694143), align with JTBD and identify broader themes.
- [x] 2. [Career dev discussion](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/68)
- [ ] 3. Prepare for hackathon: Identify issues for community contribution and follow up for implementation guide.
- [x] 4. Mckenzie Survey
- [ ] 5. 
- [x] 6. MR Reviews: only community contributions 

###  Week of 2022-09-26 - 2022-09-30 (Short week- F&F day)
- [x] 1. Assess the feedback from https://gitlab.com/gitlab-org/gitlab/-/issues/363512/ and update https://gitlab.com/groups/gitlab-org/-/epics/5071#note_1102702111
- [x] 2. Make progress on auto merge 
- [x] 3. Catch up on To-dos
- [x] 4. [CI Scaling  ](https://gitlab.com/groups/gitlab-org/-/epics/8597) - update the proposal with the data in https://docs.gitlab.com/ee/architecture/blueprints/ci_data_decay/pipeline_partitioning.html#iterations
- [x] 5. [CI/CD performance workshop](https://gitlab.com/gitlab-org/ux-research/-/issues/1727#note_1110904676)  
- [x] 6.  MR Reviews: 1. | 2. | 3. | 4. | 5. 

### Week of 2022-09-19 - 2022-09-23 (Short week- OOO on Tuesday)
- [x] 1. Respond to the Pajamas survey - https://gitlab.fra1.qualtrics.com/jfe/form/SV_e8sYGiFHmPHS7Hg?jfefe=new
- [x] 2. Make progress on auto merge 
- [x] 3. [Merge train viz](https://gitlab.com/gitlab-org/gitlab/-/issues/277391/)
- [x] 4. [CI Scaling  ](https://gitlab.com/groups/gitlab-org/-/epics/8597) - update the proposal with the data in https://docs.gitlab.com/ee/architecture/blueprints/ci_data_decay/pipeline_partitioning.html#iterations
- [x] 5. [Notificatiopn sync](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2066)  
- [x] 6.  MR Reviews: 1. | 2. | 3. | 4. | 5. 


### Week of 2022-09-12 - 2022-09-16 (This week was busy in terms of MR reviews, had to carry a few tasks forward)
- [x] 1. Move to next step: https://gitlab.com/gitlab-org/gitlab/-/issues/351147 also check with the Drupal partners on the API workflow
- [x] 2. Move to next step: https://gitlab.com/gitlab-org/gitlab/-/issues/329926 
- [x] 3. Move to next step: https://gitlab.com/gitlab-org/gitlab/-/issues/350714
- [x] 4. [CI Scaling sync](https://gitlab.com/groups/gitlab-org/-/epics/8597)
- [x] 5. [Notificatiopn sync](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2066)  
- [x] 6. MR Reviews: 1. | 2. | 3. | 4. | 5. 

###  Week of 2022-09-05 - 2022-09-09 
- [x] 1. Update the CMS Issue https://gitlab.com/gitlab-org/ux-research/-/issues/2013 and create a benchmarking issue and organise meetings
- [x] 2. Move to next step: [Notificatiopn sync](https://gitlab.com/gitlab-org/gitlab-design/-/issues/2066)  
- [x] 3. Move to next step: [CI Scaling sync](https://gitlab.com/groups/gitlab-org/-/epics/8597)
- [x] 4. Merge request reviews
- [x] 5. Move to next step: Visiting grant 
- [x] 6. Collaborate on verify designs with Nadia and Gina - [Issue](https://gitlab.com/gitlab-org/gitlab/-/issues/290011/) | Issue
- [x] 7. [Refine job token experience](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/96661)


### Week of 2022-08-29 - 2022-09-01 
- [ ] 1. Update the CMS Issue https://gitlab.com/gitlab-org/ux-research/-/issues/2013 and create a benchmarking issue and organise meetings
- [x] 2. Follow up on the community contribution guidelines https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/110299#note_1078572220 
- [x] 3. Start work on the CI scaling metrics - met with Katie and started a discussion with Matthew
- [x] 4. Start working on merge train visualization
- [x] 6. Growth and development: 
- [x] 7. Go through all the stacked to-dos
- [x] 8. Publish blog

### Week of 2022-08-22 - 2022-08-26 
- [x] 1. Update the CMS Issue https://gitlab.com/gitlab-org/ux-research/-/issues/2013 and create a benchmarking issue
- [x] 2. Conclude [Solution validation for CI?CD Project](https://gitlab.com/gitlab-org/ux-research/-/issues/2036) quota 
- [ ] 3. [15.3 milestone issues](https://gitlab.com/gitlab-org/gitlab/-/issues/361449) follow-up with Code review on the last remaining item.
- [x] 4. MR reviews
- [ ] 6. Growth and development: Read about measuring usability and work on issue https://gitlab.com/gitlab-org/gitlab/-/issues/367152
- [x] 7. Prepare and kick start the conversation around Notifications
- [x] 8. Propose an overhaul for UX community contribution guidelines 

### Week of 2022-08-15 - 2022-08-19 
- [x] 1. Update the CMS Issue https://gitlab.com/gitlab-org/ux-research/-/issues/2013 and create a benchmarking issue
- [x] 2. Research: Set up the UT test for [Solution validation for CI?CD Project](https://gitlab.com/gitlab-org/ux-research/-/issues/2036) quota 
- [x] 3. [15.3 milestone issues](https://gitlab.com/gitlab-org/gitlab/-/issues/361449) follow-up with Code review on the last remaining item.
- [x] 4. MR reviews
- [x] 6. Growth and development: Read about measuring usability and work on issue https://gitlab.com/gitlab-org/gitlab/-/issues/367152
- [x] 7. Pair with erika on publishing the blog 


### Week of 2022-08-08 - 2022-08-12 (Co-work with Emily)
- [x] 1. Update milestone plan
- [x] 2. Research: Complete the preparations for [CMS-CI Scaling](https://gitlab.com/gitlab-org/ux-research/-/issues/2013) and [Solution validation for CI?CD Project](https://gitlab.com/gitlab-org/ux-research/-/issues/2036) quota 
- [x] 3. [15.3 milestone issues](https://gitlab.com/gitlab-org/gitlab/-/issues/361449)
- [x] 4. Create and follow up on recruitment for solution validations https://gitlab.com/gitlab-org/ux-research/-/issues/1815 https://gitlab.com/gitlab-org/ux-research/-/issues/2036
- [x] 6. Growth and development: Read about measuring usability and work on issue https://gitlab.com/gitlab-org/gitlab/-/issues/367152
- [x] 7. Complete writing  blog: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/109222

### Week of 2022-08-02 - 2022-08-05 
- [ ] 1. Pajamas: 
- [x] 2. Research: Complete the discussions for [CMS-CI Scaling](https://gitlab.com/gitlab-org/ux-research/-/issues/2013) and [Solution validation for CI?CD Project](https://gitlab.com/gitlab-org/ux-research/-/issues/2036) quota 
- [x] 3. [15.3 milestone issues](https://gitlab.com/gitlab-org/gitlab/-/issues/361449)
- [x] 4. Create research recruitment issues
- [x] 6. Product design manager interview + scorecard
- [x] 7. Close all old to-do items

### Week of 2022-07-25 - 2022-06-29 
- [ ] 1. Pajamas: Re-open the links discussion
- [x] 2. Research: Complete the preparations for [CMS-CI Scaling](https://gitlab.com/gitlab-org/ux-research/-/issues/2013) and [Solution validation for CI?CD Project](https://gitlab.com/gitlab-org/ux-research/-/issues/2036) quota 
- [x] 3. [15.3 milestone issues](https://gitlab.com/gitlab-org/gitlab/-/issues/361449)
- [x] 4. Create research recruitment issues
- [x] 6. Product design manager interview + scorecard
- [x] 7. Catch up with old to-do items

### Week of 2022-07-04 - 2022-07-08 
- [x] 1. Hackathon: Create a label for internal hackathons and document UX MR review process
- [x] 2. Last two items fro KRs: https://gitlab.com/gitlab-org/gitlab/-/issues/273594 | https://gitlab.com/gitlab-org/gitlab/-/issues/337068
- [x] 3. Create 2 research issues for https://gitlab.com/gitlab-org/gitlab/-/issues/357760/ and CI Scaling 
- [x] 4. MR reviews
- [x] 6. Follow-up on laptop replacement
- [x] 7. Catch up on to-dos

### Week of 2022-06-27 - 2022-06-30 
- [x] 1. Verify hackathon week
- [x] 2. [PE 15.2 Design deliverables](https://gitlab.com/gitlab-org/gitlab/-/issues/361448)
- [x] 3. Log the expenses
- [x] 4. Merge request reviews
- [x] 6. Create issue: Designing for Scaling: https://gitlab.com/gitlab-org/gitlab/-/issues/367152
- [ ] 7. 


### Week of 2022-06-12 - 2022-06-16 
- [ ] 1. Pajamas: write blog JTBD 
- [x] 2. https://gitlab.com/gitlab-org/gitlab/-/issues/357760
- [x] 3. https://gitlab.com/gitlab-org/gitlab/-/issues/363512/
- [ ] 4. 
- [x] 6. Create issue for CI Scaling UX
- [x] 7. Feedback + MRs reviews: Issue 1 | 2 | 3 | 4 

### Week of 2022-06-12 - 2022-06-16 
- [x] 1. Pajamas: create an issue for JTBD 
- [x] 2. Evergreen ILS keynote
- [ ] 3. 
- [x] 4. Critical 15.1 issues
- [ ] 6. Create issue for CI Scaling UX
- [x] 7. Feedback + MRs reviews: Issue 1 | 2 | 3 | 4 

### Week of 2022-06-06 - 2022-06-10 (Pajamas migration day)
- [x] 1. Pajamas: Pajamas migration day MRs
- [x] 2. Record videos for new JTBDs and other findings
- [ ] 3. 15.1 issues
- [x] 4. Wind up old research issues and recruitment issues
- [ ] 6. 
- [x] 7. Feedback + MRs reviews: Issue 1 | 2 | 3 | 4 

###  Week of 2022-05-30 - 2022-06-03 Time off 
https://gitlab.com/gitlab-org/gitlab-design/-/issues/1971


### Week of 2022-05-23 - 2022-05-27 (80% capacity - F&F day)
- [ ] 1. Pajamas: 
- [x] 2. Record videos for new JTBDs and other findings
- [x] 3. --- Prep for time off ----
- [x] 4. Critical 15.0 issues
- [x] 6. Merge request interviews
- [x] 7. Feedback + MRs reviews: Issue 1 | 2 | 3 | 4 

### Week of 2022-05-16 - 2022-05-20 
- [x] 1. Pajamas: [Visual treatment for links](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/1266)
- [x] 2. User Interviews 3/9
- [x] 3. FY23 Q2 SUS S2 KR discussions
- [x] 4. Analyse interviews
- [x] 6. growth and development
- [x] 7. Feedback + MRs reviews: Issue [1](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/104255#note_945245190) | [2](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/87478#note_945255985) | [3](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/104315) | [4](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/87311#note_948556486) 

### Week of 2022-05-09 - 2022-05-13 (half capacity)
- [x] 1. User interviews
- [x] 2. Work on 15.0 design issues
- [x] 3. Kick-off recording
- [x] 4. ------Sick time off----------
- [x] 7. Feedback + MRs reviews: Issue [1](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/83680#note_941142242) | [2](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/86466#note_941535177) | [3](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/87210#note_942284893) | [4](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/83680#note_944843448) 

### Upcoming: Week of 2022-05-02 - 2022-05-06 
- [ ] 1. Pajamas: [Visual treatment for links](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/1266)
- [x] 2. Work on 15.0 design issues
- [x] 3. UX Showcase recording
- [x] 4. Coverage issues
- [x] 6. growth and development
- [x] 7. Feedback + MRs reviews: Issue [1](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/86571#note_935906939) | [2](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/86818#note_938260149) | [3](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/86880#note_938940464) | [4](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/86571#note_939748578) |  

### Week of 2022-04-25 - 2022-04-29 
- [ ] 1. Pajamas: [Visual treatment for links](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/1266)
- [x] 2. Work on 15.0 design issues
- [x] 3. Catch up on planning for [15.1](https://gitlab.com/gitlab-org/ci-cd/pipeline-execution/-/issues/98) |  [15.2](https://gitlab.com/gitlab-org/ci-cd/pipeline-execution/-/issues/101) | [15.3](https://gitlab.com/gitlab-org/ci-cd/pipeline-execution/-/issues/104) and create design planning issues
- [x] 4. [Relocation related tasks](https://about.gitlab.com/handbook/people-group/relocation/#tasks-for-team-members---relocation-approved) 
- [x] 6. Career Development Plan + Release Interviews
- [x] 7. Feedback + MRs reviews: Issue 1 | 2 | 3 | 4 

### Week of 2022-04-18 - 2022-04-22 
- [ ] 1. Documentation: Create an MR to clean UX/Engg collaboration process https://about.gitlab.com/handbook/engineering/development/ops/verify/pipeline-execution/#collaboration-with-ux-and-engineering 
- [ ] 2. Work on 15.0 design issues
- [ ] 3. Revise JTBD: https://gitlab.com/gitlab-org/ux-research/-/issues/1900 
- [ ] 4. Interview release team candidate 
- [ ] 5. DMBA: Designing your own impact   
- [ ] 6. Feedback + MRs reviews: Issue [1](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/83224#note_915619840) | 2 | 3 | 4 

### Week of 2022-04-11 - 2022-04-15 
- [ ] 1. catch up on to-dos 
- [ ] 2. Work on 14.10 design issues m/gitlab-org/gitlab/-/issues/351575
- [ ] 3. work on career development plan
- [ ] 4. Close out auto-merge solution validation issues 
- [ ] 6. Cover for Nadia (PA) 
- [ ] 7. Feedback + MRs reviews: Issue [1](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/83751#note_909838559) | [2](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/83680#note_909890476) | [3](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/83969#note_906211826) | [4](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/83751#note_910953173) 
- [ ] 8 . D.MBA - Metrics

###  Week of 2022-04-04 - 2022-04-08 
- [ ] 1. Verify release PD interviews 
- [ ] 2. Work on 14.10 design issues m/gitlab-org/gitlab/-/issues/351575
- [ ] 3. Participate in `Restructure MRs` workshop https://gitlab.com/gitlab-org/gitlab/-/issues/355468
- [ ] 4. Revisit [JTBDs](https://about.gitlab.com/handbook/engineering/development/ops/verify/pipeline-execution/jtbd/) and [pipelines conceptual model](https://www.figma.com/file/J68bePHXIN5OPWqaFFY9ri/branch/hvP38jNynJCXPxN3ODgK2W/Conceptual-model) 
- [ ] 6. GDD: D-MBA - Design strategy and Assignment #2
- [ ] 7. Feedback + MRs reviews: Issue [1](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/84226) | [2](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/83224) | 3 | 4 

### _ Week of 2022-03-21 - 2022-03-25 
- [x] 1. Put up a plan for aggregated pipeline duration: https://gitlab.com/groups/gitlab-org/-/epics/4915#note_840455248 https://gitlab.com/gitlab-org/gitlab/-/issues/357528
- [ ] 2. Work on 14.10 design issues m/gitlab-org/gitlab/-/issues/351575
- [x] 3. Assign [UX coverage issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1909) to team-members
- [x] 4. Prepare for participation in https://gitlab.com/gitlab-org/gitlab/-/issues/355468#note_882286433 
- [x] 6. GDD: D-MBA - Design strategy and Assignment #2
- [x] 7. Feedback + MRs reviews: Issue [1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/merge_requests/2742#note_881486752) | [2](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/67776#note_886716559) | 3 | 4 

### Week of 2022-03-14 - 2022-03-18 
- [x] 1. Collaboration: Work with Gina on labels issue
- [x] 2. Milestone issues: [final follow-up](https://gitlab.com/gitlab-org/gitlab/-/issues/346995)
- [x] 3. Record kick-off with James
- [x] 4. Design system: 
     - [x] follow up and close on currently open MRs for Pajamas and update on UX channel
- [x] 6. GDD: GDD: D-MBA - Business empathy and Assignment #1
- [x] 7. Feedback + MRs reviews: Issue [1](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/82458) | [2](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/99318) | [https://gitlab.com/gitlab-org/gitlab/-/merge_requests/82907] | [4](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/82539) | [5](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/83048#note_878652203) 



### Week of 2022-03-07 - 2022-03-11 
- [x] 1. Research: Draft: [Setup tracking for pipeline index page actions](https://gitlab.com/gitlab-org/gitlab/-/issues/354751)
- [x] 2. Milestone issues: [x/5](https://gitlab.com/gitlab-org/gitlab/-/issues/346995) 
- [x] 3. Research: 
     - [x] [Relook at old research data around pipeline observability](https://dovetailapp.com/projects/1nLmXoz4lDFujwV7RUoZpd/highlights#v=ewAiAHEAIgA6ACIAIgB9AA)
- [x] 4. Design system: follow up on
     - [x] [feat(ResponsiveFirst): Planning ahead for spaces in layout](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/merge_requests/2742#note_859743896)
     - [x] [Make progress on Pipelines object model](https://www.figma.com/file/J68bePHXIN5OPWqaFFY9ri/branch/hvP38jNynJCXPxN3ODgK2W/Conceptual-model?node-id=5847%3A553)
- [x] 6. GDD: Learn about content reflow 
- [x] 7. Feedback + MRs reviews: Issue [https://gitlab.com/gitlab-org/gitlab/-/merge_requests/82236#note_870418790] | [https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/99318#note_870280350] | [3](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/82521#note_871793205)

### _Week of 2022-02-28 - 2022-03-04 
- [x] 1. Interview training: [Lead an interview and complete the training](https://gitlab.com/gitlab-com/people-group/Training/-/issues/1062)
- [x] 2. Milestone issues: [x/5](https://gitlab.com/gitlab-org/gitlab/-/issues/346995) https://gitlab.com/gitlab-org/gitlab/-/issues/225430
- [x] 3. Documentation: 
     - [ ] [Update the description for actionable insights](https://gitlab.com/dashboard/merge_requests?assignee_username=v_mishra#:~:text=Update%20the%20description%20for%20actionable%20insights)
     - [ ] tbd
- [x] 4. Design system: 
      - [ ] [feat(ResponsiveFirst): Planning ahead for spaces in layout](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/merge_requests/2742#note_859743896)
      - [x] [Make progress on Pipelines job model](https://www.figma.com/file/J68bePHXIN5OPWqaFFY9ri/branch/hvP38jNynJCXPxN3ODgK2W/Conceptual-model?node-id=5847%3A553)
- [x] 6. GDD: Read new blogs on CI/CD https://towardsdev.com/basics-of-ci-cd-with-gitlab-d4e2ba23440e
- [x] 7. Feedback + MRs reviews: Issue [1](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/99499) | [2](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/81568) | [3](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/81476) | [4](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/99387) 

### Week of 2022-02-21 - 2022-02-25 🌟 Hackathon week 🌟 
- [x] 1. Collaboration: With Code review team on [re-organizing merge strategy](https://gitlab.com/gitlab-org/gitlab/-/issues/336631/)
- [x] 2. Milestone issues: [x/5](https://gitlab.com/gitlab-org/gitlab/-/issues/346995)
- [x] 3. Documentation: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/99318
- [x] 4. Design system: 
     - [x] [Start exploring pipeline object model](https://www.figma.com/file/J68bePHXIN5OPWqaFFY9ri/branch/hvP38jNynJCXPxN3ODgK2W/Conceptual-model)
     - [x] [Layout>>Responsive design](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/merge_requests/2696)
- [x] 6. GDD: --- F&F day
- [x] 7. Feedback + MRs reviews: Issue [1](https://gitlab.com/gitlab-org/gitlab/-/issues/353333/designs/6.png#note_849190858) | [2](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/81049#note_851204510) | [3](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/81325#note_853206208) | [4](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/81476) | 5

### Week of 2022-02-13 - 2022-02-18 
- [x] 1. Collaboration: Runner issues and usage quota related issues
- [x] 2. Milestone issues: [x/4](https://gitlab.com/gitlab-org/gitlab/-/issues/351574)
- [x] 3. Follow-up: https://gitlab.com/gitlab-org/gitlab-design/-/issues/1855 
- [x] 4. Design system: 
     - [x] [Start exploring pipeline object model](https://www.figma.com/file/J68bePHXIN5OPWqaFFY9ri/branch/hvP38jNynJCXPxN3ODgK2W/Conceptual-model)
     - [x] [Layout>>Responsive design](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/merge_requests/2696)
- [x] 6. Relocation: get in touch with PeopleOps and initiate the process
- [x] 7. Feedback + MRs reviews: Issue [1](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/81049) | [2](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/79470) | [3](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/81032#note_848272361) 

### Week of 2022-02-07 - 2022-02-11 
- [x] 1. Collaboration: make progress with https://gitlab.com/gitlab-org/gitlab/-/issues/340392, collaborate with Mike N. on https://gitlab.com/gitlab-org/gitlab/-/issues/351474
- [x] 2. Milestone issues: [3/3](https://gitlab.com/gitlab-org/gitlab/-/issues/346995)
- [x] 3. Documentation:  PE UX
- [x] 4. Design system: Pipeline object, https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/merge_requests/2696
- [x] 6. GDD:
    - [x] Learn more about merge strategies
    - [x] Annual career development plan FY23
- [x] 7. Feedback + MRs reviews: Issue 1 | 2 | 3 | 4 




### Week of 2022-01-31 - 2022-02-04  
- [x] 1. Collaboration: Collaborate with Nadia and Gina to understand the current priorities across verify
- [x] 2. 14.8 Milestone issues: [2/3](https://gitlab.com/gitlab-org/gitlab/-/issues/346996)
- [ ] 3. Document: Update [PE UX page](https://about.gitlab.com/handbook/engineering/ux/stage-group-ux-strategy/ci-cd/pipeline-execution/)
- [x] 4. Design system: 
    - [x] Watch: [Move and rename Figma projects](https://www.youtube.com/watch?v=DMyZBD3__S4&ab_channel=GitLabUnfiltered)
    - [x] Watch:[ Figma for GitLab - teams + projects](https://www.youtube.com/watch?v=fOTbhxTnmh0&ab_channel=GitLabUnfiltered)
- [x] 6. GDD: Improve research skills
    - [x]  Participate in [interview carousel](https://gitlab.com/gitlab-org/ux-research/-/issues/1714)
    - [x] Research Design](https://www.coursera.org/learn/research-inquiry-discovery/home/welcome)
- [x] 7. Feedback + MRs reviews: Issue [1](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/79888) | [2](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/79763) | [3](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/79614)  | [4](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/79969)



### Week of 2022-01-24 - 2022-01-28 (Public holiday on the 26th Jan) 
- [x] 1. Collaboration: Pedro - [Auto-merge](https://gitlab.com/gitlab-org/gitlab/-/issues/340392) | [RBAC User flow designs and validation](https://gitlab.com/gitlab-org/gitlab/-/issues/350192)
- [x] 2. 14.8 Milestone issues: [Work on summarizing the next steps for auto-merge](https://gitlab.com/gitlab-org/gitlab/-/issues/340392)
- [x] 3. Highlight blocking issues [FigJam](https://www.figma.com/file/yzghoe7hbYvhjeHg32eYuQ/Untitled?node-id=0%3A1)
- [x] 4. Research: Job filters
- [x] 6. GDD: Coursera - [Research Design](https://www.coursera.org/learn/research-inquiry-discovery/home/welcome)
- [x] 7. Feedback + MR reviews: [1](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/79242) | [2](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/78671) | [3](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/78722) | [4](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/78974) | [5](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/72545) | [6](https://gitlab.com/gitlab-org/gitlab/-/issues/30101/designs/Pre-filled_variables_from_.gitlab-ci.yml.jpg#note_812202699)
- [x] 8. Pajamas issue: [Follow-up on Layouts improvements](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/merge_requests/2696)
- [ ] 9. Capacity planning + Career growth plan for FY23-Q1





### Week of 2022-01-17 - 2022-01-22 
- [x] 1. Collaboration: https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/merge_requests/2667
- [x] 2. 14.7 Milestone issues: https://gitlab.com/gitlab-org/gitlab/-/issues/346298 - Make bot workflows more secure
- [x] 3. Document: Add severity labels to the [SUS impacting issues in Pipeline Execution](https://gitlab.com/gitlab-org/ci-cd-ux/-/issues/59)
- [x] 4. Research: Job index page filters - prepare
- [ ] 6. Update the research outcome from [auto-merge research](https://gitlab.com/gitlab-org/ux-research/-/issues/1568)
- [ ] 7. [Open design dialogue](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/-/issues/1236) - post update

---------------
HNY 2022
---------------

### Week of 2021-12-13 - 2021-12-16 
- [x] 1. Prep for time off https://gitlab.com/gitlab-org/gitlab-design/-/issues/1792
- [ ] 2. Milestone: [Make sure all designs are added to issues and update progress](https://gitlab.com/gitlab-org/gitlab/-/issues/343337) 
- [ ] 3. Pajamas: Attend Office hours
- [ ] 4. Research: Meet with Erika to discuss the progress on ongoing research efforts


### Week of 2021-11-06 - 2021-12-10 
- [x] 1. Collaboration: Jeremy Elder/Sunjung
- [x] 2. Milestone: [Milestone 14.6 issues](https://gitlab.com/gitlab-org/gitlab/-/issues/343337) 
- [x] 3. Review community MRs from hackathon [1](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/75810) 
- [x] 4. Research: Auto-merge started to prep with Erika
- [x] 6. Ops Think Big: Conclude
- [x] 7. Feedback: Issue 1 | 2 | 3 | 4 
- [ ] 8. Stretch: Pajamas issue

### Week of 2021-11-29 - 2021-12-03 
- [x] 1. Collaboration: katie::package
- [x] 2. Milestone: follow-up on feedback
- [x] 3. Provide Feedback: Issue 1 | 2 | 3 | 4 
- [x] 4. UX CI/CD meeting
- [x] 5. Prep for Ops Think Big: https://gitlab.com/gitlab-org/gitlab/-/issues/346881 
- [x] 6. Tabular layout

### Week of 2021-11-22 - 2021-11-26 
- [x] 1. Collaboration: Sunjung::Pajamas | Daniel::tabular layout 
- [x] 2. Milestone: [3/5](https://gitlab.com/gitlab-org/gitlab/-/issues/343337) Milestone 14.6 issues
- [x] 3. Document: prepare for Think Big Ops - Secrets handling (Incomplete documentation)
- [ ] 4. Research: Auto-merge [research](https://gitlab.com/gitlab-org/ux-research/-/issues/1568)
- [x] 6. GDD: Read 'Design Justice'
- [x] 7. Feedback: Issue 1 | 2  
- [ ] 8. Stretch: Coverage for Gina



### Week of 2021-11-15 - 2021-11-19 
- [x] 1. Milestone: Kickoff + planning 
- [ ] 2. Document: Cross-stage efforts on PE UX page
- [x] 3. Stage-group: Create issues from Pipeline Index solution validation findings 
- [ ] 4. Stretch: Research: Start prep for Auto-merge research
- [x] 5. Stretch: Feedback: Issue 1 | 2 
- [x] 6. Attend Contribute 🌟 
- [x] 7. Coverage for Nadia
- [ ] 8. Pajamas: Work on [MR](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/merge_requests/2627)


------
### Week of 2021-11-09 - 2021-11-14 
- [x] 1. Performance factors
- [x] 2. Milestone: 14.5 3 issues
- [x] 3. Research: Analyse research interviews
- [x] 4. GDD: Research methodologies readings
- [x] 5. Stretch: Feedback: Issue [1](https://gitlab.com/gitlab-org/gitlab/-/issues/337615) | 2 
- [x] 6. Housekeeping: Update PE JTBDs + Coverage issues

### Week of 2021-11-02 - 2021-11-06 
- [ ] 1. Housekeeping: MR for solution validation process updates
- [x] 2. Milestone: [14.5 Issues](https://gitlab.com/gitlab-org/gitlab/-/issues/336562) - 2 issues + follow-ups
- [ ] 3. Document: Severity label application
- [x] 4. Research: 5 Interviews - [Dovetail](https://dovetailapp.com/projects/5dIkLDWFXWJ9ZqUvq2fMPR/readme) 
- [ ] 6. ~~Pajamas: [Explore table component limitations](https://gitlab.com/gitlab-org/gitlab/-/issues/340029)~~
- [ ] 7. Feedback: Issue [1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/1120) | [2 ](https://gitlab.com/gitlab-org/gitlab/-/issues/342138)
- [x] ~~8. Stretch: GDD: Read the book: Crossing the chasm(2-4 chapters)~~


### Week of 2021-10-25 - 2021-10-29 
- [x] 1. Housekeeping: Update PE UX page https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/92617 
- [x] 2. Milestone: [14.5 Issues](https://gitlab.com/gitlab-org/gitlab/-/issues/336562)- 2 issues
- [x] 3. Document: Additional L&D resources https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/92960 
- [x] 4. Research: Interviews for [`Layout changes for the pipeline index page`](https://gitlab.com/gitlab-org/ux-research/-/issues/1584) - Recruitment wasn't a success
- [x] 6. GDD: learn more director level JTBDs for PE [Dovetail link](https://dovetailapp.com/projects/5slzPzsDgvTdip4O0umGos/data/b/6a1aGmv5Bi71z7o6jGelmt)
- [x] 7. Feedback: Issue [1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/1120) | [2](https://gitlab.slack.com/archives/CLW71KM96/p1635180155036700) 
- [x] 8. Pajamas: [Created MR for show/hide icons](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/merge_requests/2610)


### Week of 2021-10-18 - 2021-10-22 
- [-] 1. Onboarding buddy check-in with Gina
- [x] 2. Catch-up on tags on issues
- [x] 3. Start investigation on 14.5 issues
- [x] 4. Complete discussion guide for [solution validation](https://gitlab.com/gitlab-org/ux-research/-/issues/1584) and complete recritment
- [x] 6. Explore limitations of current table component [Issue](https://gitlab.com/gitlab-org/gitlab/-/issues/340029)
- [x] 7. Record GDK good practices video with Jose
- [x] 8. Submit an MR for process change around UX debt for PE
- [x] 9. GDD: Learn more around design system frameworks
- [-] 10. Spill over:Q4 planning + goal setting

###  Week of 2021-10-11 - 2021-10-14 (F&F Day on 15th Oct) 
- [x] 1. UX Showcase presentation
- [x] 2. Onboarding buddy check-in with Gina
- [x] 3. 14.5 Kick-off
- [ ] 4. Q4 planning + goal setting
- [x] 6. Prioritize PE 14.4 issue:  https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/73172 and wrap 14.4
- [x] 7. Shadow an SMB interview
- [x] 8. Pipeline index page solution validation recruitment + guidelines
- [x] 9. Present in the GitLab 10th anniversary community event
- [x] 10. Stretch: Attend Figma Schema

### Week of 2021-10-04 - 2021-10-08 
- [x] 1. Add a section for cross-stage work on PE UX page
- [x] 2. Review community MRs https://gitlab.com/gitlab-org/gitlab/-/merge_requests/68549#note_696025128 | https://gitlab.com/gitlab-org/gitlab/-/merge_requests/55744#note_697607035
- [x] 3. Work on 14.4 issues
- [x] 4. Respond to tags on issues and MRs
- [x] 6. Close coverage issue
- [x] 7. Shadow an SMB interview
- [x] 8. Review FE + UX MRs https://gitlab.com/gitlab-org/gitlab/-/merge_requests/71687#note_697990490 | https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/73172
- [ ] 9. Attend Figma Schema
- [ ] 10. Onboarding buddy check-in with Gina

### Week of 2021-10-01 - 2021-10-01 (catch-up on tags and notifications)
- [x] Catch up on notifications and plan the upcoming week.

### eek of 2021-09-12 - 2021-09-17 
- [x] 1. Docs contribution tool [Issue](https://gitlab.com/gitlab-org/verify-stage/-/issues/106)
- [x] 2. SMB Golden journey research(Interview)
- [x] 3. Prepare for time off [Issue](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1708)
- [x] 4. [Return warnings in job logs when CI_JOB_TOKEN is used without the secure workflow being enabled
](https://gitlab.com/gitlab-org/gitlab/-/issues/339791)
- [ ] 6. Watch permissions video by Daniel and nick [Video](https://gitlab.slack.com/archives/CLW71KM96/p1631123529032800?thread_ts=1631030935.028900&cid=CLW71KM96)
- [x] 7. Onboarding check-in with Gina
- [ ] 8. Write a blog proposal for CI minutes
- [ ] 9. Prepare for [Solution validation of pipeline index page](https://gitlab.com/gitlab-org/gitlab/-/issues/338224)
- [x] Create pajamas issues for charts empty states




###  Week of 2021-09-05 - 2021-09-010 
- [ ] 1. Revisit permission mapping for PE
- [x] 2. Onboarding buddy check-in with Gina
- [x] 3. Follow-up on heuristic buddy evaluation task and create recommendations
- [x] 4. 14.4 Kick-off Video
- [x] 6. Follow-up on Auto merge strategy
- [x] 7. Follow up on 14.3 issues
- [x] 8. Create Auto-merge Epic + issues
- [x] 9. Coverage issue for time off
- [x] 10. Follow-up on PE UX handbook page MR
- [ ] 11.


###  Week of 2021-08-28 - 2021-09-02 
- [x] 1. 360 reviews
- [x] 2. 14.3 PE design issues + https://gitlab.com/gitlab-org/gitlab/-/issues/336986 https://gitlab.com/gitlab-org/gitlab/-/issues/339230 
- [x] 3. Onboarding buddy check-in with Gina
- [ ] 4. Stretch: Follow-up on heuristic buddy evaluation task
- [x] 5. Ready 14.4 planning issue 
- [x] 6. Stretch: Read about GitLab product principles https://about.gitlab.com/handbook/product/product-principles/#multi-feature-usage-adoption-journey  
- [ ] 7. 

###  Week of 2021-08-23 - 2021-08-24 (PTO from 08/25 - 08/27)
- [x] 1. Training - 10 Ways to Avoid Phishing Scams 
- [x] 2. Check with heuristic buddy 
- [x] 3. Work on https://gitlab.com/gitlab-org/gitlab/-/issues/332640 and have Gina shadow
- [x] 4. https://gitlab.com/gitlab-org/gitlab/-/issues/325529 
- [x] 5. Work on: https://gitlab.com/gitlab-org/gitlab/-/issues/299907
- [x] 6. Stretch: Write 1 360 review 
- [x] 💉 Get vaccinated 💉


###  Week of 2021-08-16 - 2021-08-20
- [x] 1. Check the status for https://gitlab.com/gitlab-org/gitlab/-/issues/207988 https://gitlab.com/gitlab-org/gitlab/-/issues/326216 https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/845
- [ ] 2. Work on the first design draft for https://gitlab.com/gitlab-org/gitlab/-/issues/325529  
- [x] 3. Onboarding buddy check-in with Gina
- [x] 4. MR: Update Pipeline execution UX page in handbook
- [x] 5. Discover more quick win FE issues to share with the team. try to align with https://gitlab.com/groups/gitlab-org/-/epics/6413 
- [x] 6. Respond to https://gitlab.com/gitlab-org/gitlab/-/issues/336402#note_644374908 
- [x] 7. Prepare for heuristic buddy UX scorecard and handover the JTBD to Becka 2/2
- [x] 8. Follow-up on feedback on 14.2 issues 
- [x] 9. Watch the video for Pajamas playground by Daniel Fosco + recent UX showcase videos
- [x] 10. Record a walkthrough of creating and sharing a testing env using GitPod. 
- 11. Stretch: (no stretch task was worked on in this week)



###  Week of 2021-08-09 - 2021-08-13 
- [x] 1. Prepare for Kick-off recording
- [x] 2. Follow-up on MRs for `in dev` work for milestone
- [x] 3. Onboarding buddy check-in with Gina
- [x] 4. Prepare for heuristic buddy UX scorecard and handover the JTBD to Becka 1/2
- [x] 5. Ready 14.3 planning issue 
- [x] 6. Provide designs for https://gitlab.com/gitlab-org/gitlab/-/issues/337496
- [x] 7. Provide designs for https://gitlab.com/gitlab-org/gitlab/-/issues/334131 
- [x] 8. Prepare for hosting Ops Think Big session
- [x] 9. Research check-in - [What supports and fuels the SMB/MM pursuit of the Golden Journey?](https://gitlab.com/gitlab-org/ux-research/-/issues/1522)
- [x] 10. Attend Live Learning: Individual Growth Plan
- [x] 11. Watch [How Atlassian Uses Advanced Roadmaps](https://www.youtube.com/watch?v=fz7GthQSP0o&ab_channel=Atlassian)

### Week of 2021-08-02 - 2021-08-07 
- [x] 1. Refine Q3 Planning issue []
- [x] 2. Onboarding check-in with Gina
- [x] 3. Put first draft of proposal for [Improve MR widget warning when pipeline is skipped and merging is allowed](https://gitlab.com/gitlab-org/gitlab/-/issues/330850)
- [x] 4. Put first draft of proposal for [Invalid gitlab-ci.yaml ignores [ci-skip]](https://gitlab.com/gitlab-org/gitlab/-/issues/235855)
- [x] 5. Create an MR to refine Pipeline Execution JTBDs
- [x] 6. Buddy calls
- [x] 7. Comment on issue: https://gitlab.com/gitlab-org/gitlab/-/issues/332040 
- [x] 8. Create MR to add show/hide pattern to pajamas
- [x] 9. Create an issue for 14.4 design planning
- [x] 10. Watch [`How to develop your career plan`](https://www.linkedin.com/learning/how-to-develop-your-career-plan/introduction?u=2255073) on inkedIn learning
