Repo: Penpot/Penpot
Access: Guest
Browser: Chrome. Did not work on Firefox. I don't have an extension for loom so I don't know why this happened.

I work with an open source prototyping tool - Penpot for my personal projects. I always create an issue for any bug or feature request I want to log on their repository.

1. While writing a description for an issue, an option appears to `Record a loom`

| image |
|-------|
| ![Screen_Shot_2021-09-13_at_2.19.33_PM](/uploads/c4489246f7f4d5149c30b919836a96d2/Screen_Shot_2021-09-13_at_2.19.33_PM.png) |

2. Clicking on `Record a loom` starts a Loom recording

| image |
|-------|
| ![Screen_Shot_2021-09-15_at_12.05.01_PM](/uploads/64b67274aee4a6e097a3a1639775205e/Screen_Shot_2021-09-15_at_12.05.01_PM.png) |

3. When the recording is completed(click on ✅), a modal appears with preview

| image |
|-------|
| ![Screen_Shot_2021-09-15_at_12.05.27_PM](/uploads/cce5d4ecbca0c5b277254273fb4806b0/Screen_Shot_2021-09-15_at_12.05.27_PM.png) |

4. Upon confirmation, the loom link gets added to the description

| image |
|-------|
| ![Screen_Shot_2021-09-15_at_12.19.26_PM](/uploads/ff1d847166525a5110c00c309144b977/Screen_Shot_2021-09-15_at_12.19.26_PM.png) |
